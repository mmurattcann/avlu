<?php
return [
    "logo" => [
        "width" => 250,
        "height" => 250
    ],
    "blog" => [
        "cover" => [
            "width"  => 870,
            "height" => 450
        ],
        "thumbnail" => [
            "width" => 412,
            "height" => 250
        ]
    ],
    "product" => [
        "width" => 870,
        "height" => 920
    ],
    "slider" => [
        "width" => 1920,
        "height" => 700
    ],
    "box" => [
        "width" => 570,
        "height" => 320
    ]
];
