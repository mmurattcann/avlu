<?php
return [
    "normalButtons" => [
        "btn-success",
        "btn-secondary",
        "btn-danger",
        "btn-primary",
        "btn-warning",
        "btn-info",
    ],
    "outlineButtons" => [
        "btn-outline-success",
        "btn-outline-secondary",
        "btn-outline-danger",
        "btn-outline-primary",
        "btn-outline-warning",
        "btn-outline-info",
    ]

];
