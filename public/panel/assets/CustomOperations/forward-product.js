$(".is_featured").change(function (){

    var is_featured = $(this).prop('checked') === true ? 1 : 0;
    var id = $(this).data('id');
    var $route = $(this).data("route");

    $.ajax({
        url: $route,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        dataType: "json",

        data: {'is_featured': is_featured, "id": id },

        success: function (data) {
            toastr.options.closeButton = false;
            toastr.options.closeMethod = 'fadeOut';
            toastr.options.closeDuration = 100;
            toastr.success(data.message);

        }
    });
});
