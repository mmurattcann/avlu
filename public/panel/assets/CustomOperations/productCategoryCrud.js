$(function() {
    $('.is_active').change(function () {

        var is_active = $(this).prop('checked') === true ? 1 : 0;
        var id = $(this).data('id');
        var $route = $(this).data("route");

        $.ajax({
            url: $route,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            dataType: "json",

            data: {'is_active': is_active, "id": id},

            success: function (data) {
                toastr.options.closeButton = false;
                toastr.options.closeMethod = 'fadeOut';
                toastr.options.closeDuration = 100;
                toastr.success(data.message);

            }
        });
    });

    $(".table tbody").on("click",".delete-button", function () {

        var id = $(this).data("id");
        var $tr = $("#tr-"+id);
        var $route = $(this).data("route");
        swal({
                title: 'Silmek istediğinize emin misiniz?',
                text: "Bilgiler kalıcı olarak silinecek!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Evet, sil!',
                cancelButtonText: 'Vazgeç!',
                reverseButtons: true
            },
            function(isConfirm) {
                if (isConfirm) {

                    var token = $("meta[name='csrf-token']").attr("content");
                    $.ajax({
                        url: $route,
                        type: 'DELETE',
                        data: {
                            "id": id,
                            "_token": token,
                        },
                        success: function (data) {
                            $("#users-table tbody").find($tr).fadeOut(1000);

                            toastr.options.closeButton = false;
                            toastr.options.preventDuplicates = false;
                            toastr.options.closeMethod = 'fadeOut';
                            toastr.options.closeDuration = 100;
                            toastr.success(data.message);
                        }
                    });
                }
            }
        )
    })
});
