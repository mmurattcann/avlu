function successToast($message){

    toastr.options.closeButton = false;
    toastr.options.closeMethod = 'fadeOut';
    toastr.options.closeDuration = 100;
    toastr.success($message);

}
function errorToast($message){
    toastr.options.closeButton = false;
    toastr.options.closeMethod = 'fadeOut';
    toastr.options.closeDuration = 100;
    toastr.error($message);
}

function warningToast($message){
    toastr.options.closeButton = false;
    toastr.options.closeMethod = 'fadeOut';
    toastr.options.closeDuration = 100;
    toastr.warning($message);
}
function block($element){

    $($element).block({
        css: {
            border: 'none',
            padding: '15px',
            "font-size": '17px',
            "background-color": 'transparent',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff',

        } ,
        message: 'Lütfen Bekleyiniz...',

    });
}

function blockFullPage(){

    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            "font-size": '17px',
            "background-color": '#bd0000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff',

        } ,
        message: 'Lütfen Bekleyiniz...',

    });
}
function unBlock($element){

    setTimeout(function(){
        $($element).unblock();
    }, 1250);


}
function unBlockFullPage($delay = 1250){

    setTimeout(function(){
        $.unblockUI();
    }, $delay);


}


$(".addCart").click(function (){

    var productId = $(this).data("id");
    var route = $(this).data("route");

    $.ajax({
        url: route,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        dataType: "json",

        data: {
            "productId" : productId
        },
        beforeSend:function (){
            blockFullPage();
        },
        success: function (data) {

            if(data.status === 201)
            {
                unBlockFullPage();
                successToast(data.message);
            }
            else{
                unBlockFullPage();
                errorToast(data.message)
            }
        }
    });
});
