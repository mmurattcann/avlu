<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("title");
            $table->string("slug");
            $table->text("description");
            $table->string("route");
            $table->integer("rank")->default(1);
            $table->tinyInteger("is_active");
            $table->unsignedBigInteger("parent_id")->nullable()->default(0);
            $table->timestamps();

            $table->foreign("parent_id")
                ->references("id")
                ->on("menus")
                ->onDelete("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
