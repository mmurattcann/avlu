<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("site_title");
            $table->string("logo")->nullable();
            $table->string("favicon")->nullable();
            $table->string("phone")->nullable();
            $table->string("gsm");
            $table->string("contact_email");
            $table->string("info_email");
            $table->text("address");
            $table->text("google_map")->nullable();
            $table->string("site_key")->nullable();
            $table->string("secret_key")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('options');
    }
}
