<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogCommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "content" => "İlk Yorum",
                "status"  => 1,
                "blog_id" => 1,
                "user_id" => 1,
                "comment_id" => null
            ],
            [
                "content" => "İkinci Yorum",
                "status"  => 1,
                "blog_id" => 1,
                "user_id" => 1,
                "comment_id" => 1,
            ],
        ];

        DB::table("blog_comments")->insert($data);
    }
}
