<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "title" => "Alışveriş",
                "slug"  => "alisveris",
                "description" => "Alışveriş kategorisine ait blog yazıları.",
                "is_active" => 1
            ],
            [
                "title" => "Güvenlik",
                "slug"  => "guvenlik",
                "description" => "Güvenlik kategorisine ait blog yazıları.",
                "is_active" => 1
            ],
        ];

        DB::table("blog_categories")->insert($data);
    }
}
