<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "title" => "Test Blog",
                "slug"  => "test-blog",
                "image" => null,
                "description" => "Test Blog başlıklı blog için SEO açıklama metni.",
                "content"     => "Test Blog İçeriği",
                "is_active"   => 1,
                "user_id"     => 1,
                "category_id" => 1,
            ],
            [
                "title" => "Test Blog 2",
                "slug"  => "test-blog-2",
                "image" =>  null,
                "description" => "Test Blog 2 başlıklı blog için SEO açıklama metni.",
                "content"     => "Test Blog 2 İçeriği",
                "is_active"   => 1,
                "user_id"     => 1,
                "category_id" => 2,
            ]
        ];

        DB::table("blogs")->insert($data);

    }
}
