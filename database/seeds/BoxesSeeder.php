<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BoxesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("boxes")->truncate();

        $data = [
            [
                "image" => "RESIM",
                "title" => "Kutu 1",
                "description" => "Kutu 1 için açıklama metni",
                "route" => "https://facebook.com",
                "button_text" => "Hemen İncele",
                "rank" => 1,
                "is_active" => 1
            ],
            [
                "image" => "RESIM",
                "title" => "Kutu 2",
                "description" => "Kutu 2 için açıklama metni",
                "route" => "https://twitter.com",
                "button_text" => "Keşfet",
                "rank" => 2,
                "is_active" => 1
            ],

        ];

        DB::table("boxes")->insert($data);
    }
}
