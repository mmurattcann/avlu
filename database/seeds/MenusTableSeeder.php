<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = DB::table("menus");

        $table->truncate();

        $data = [
            [
                "title" => "Menü 1",
                "slug" => "menu-1",
                "description" => "Menü 1 İçin Açıklama Metni",
                "route" => "#",
                "rank" => 1,
                "is_active" => 1,
                "parent_id" => null
            ],
            [
                "title" => "Menü 2",
                "slug" => "menu-2",
                "description" => "Menü 2 İçin Açıklama Metni",
                "route" => "#",
                "rank" => 2,
                "is_active" => 1,
                "parent_id" => null
            ],
            [
                "title" => "Menü 3",
                "slug" => "menu-3",
                "description" => "Menü 3 İçin Açıklama Metni",
                "route" => "#",
                "rank" => 3,
                "is_active" => 1,
                "parent_id" => null
            ],
            [
                "title" => "Alt Menü 1.1",
                "slug" => "alt-menu-1-1",
                "description" => "Alt Menü 1 İçin Açıklama Metni",
                "route" => "#",
                "rank" => 1,
                "is_active" => 1,
                "parent_id" => 1
            ],
            [
                "title" => "Alt Menü 1.2",
                "slug" => "alt-menu-1-2'",
                "description" => "Alt Menü 2 İçin Açıklama Metni",
                "route" => "#",
                "rank" => 2,
                "is_active" => 1,
                "parent_id" => 1
            ],
        ];

        $table->insert($data);
    }
}
