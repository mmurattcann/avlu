<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FooterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\Schema::disableForeignKeyConstraints();

        DB::table("footer_columns")->truncate();

        $data = [
            [
                "title" => "Kolon 1",
                "rank" => 1,
                "is_active" => 1
            ],
            [
                "title" => "Kolon 2",
                "rank" => 2,
                "is_active" => 2
            ],
            [
                "title" => "Kolon 3",
                "rank" => 3,
                "is_active" => 3
            ],
            [
                "title" => "Kolon 4",
                "rank" => 4,
                "is_active" => 3
            ],
            [
                "title" => "Kolon 5",
                "rank" => 5,
                "is_active" => 3
            ],
            [
                "title" => "Kolon 6",
                "rank" => 6,
                "is_active" => 3
            ]
        ];

        DB::table("footer_columns")->insert($data);

        DB::table("footer_items")->truncate();

        $itemData = [
            [
                "title" => "Satır 1",
                "slug" => "satir-1",
                "description" => "Satır 1 açıklaması",
                "route" => "https://google.com",
                "rank" => 1,
                "is_active" => 1,
                "parent_id" => 1
            ],
            [
                "title" => "Satır 2",
                "slug" => "satir-2",
                "description" => "Satır 2 açıklaması",
                "route" => "https://google.com",
                "rank" => 2,
                "is_active" => 1,
                "parent_id" => 1
            ],
            [
                "title" => "Satır 3",
                "slug" => "satir-3",
                "description" => "Satır 3 açıklaması",
                "route" => "https://google.com",
                "rank" => 3,
                "is_active" => 1,
                "parent_id" => 1
            ],
            [
                "title" => "Satır 1",
                "slug" => "satir-1",
                "description" => "Satır 1 açıklaması",
                "route" => "https://google.com",
                "rank" => 1,
                "is_active" => 1,
                "parent_id" => 2
            ],
            [
                "title" => "Satır 1",
                "slug" => "satir-1",
                "description" => "Satır 1 açıklaması",
                "route" => "https://google.com",
                "rank" => 1,
                "is_active" => 1,
                "parent_id" => 3
            ],
            [
                "title" => "Satır 1",
                "slug" => "satir-1",
                "description" => "Satır 1 açıklaması",
                "route" => "https://google.com",
                "rank" => 1,
                "is_active" => 1,
                "parent_id" => 4
            ],
            [
                "title" => "Satır 1",
                "slug" => "satir-1",
                "description" => "Satır 1 açıklaması",
                "route" => "https://google.com",
                "rank" => 1,
                "is_active" => 1,
                "parent_id" => 5
            ],
            [
                "title" => "Satır 1",
                "slug" => "satir-1",
                "description" => "Satır 1 açıklaması",
                "route" => "https://google.com",
                "rank" => 1,
                "is_active" => 1,
                "parent_id" => 6
            ],
            [
                "title" => "Satır 2",
                "slug" => "satir-2",
                "description" => "Satır 2 açıklaması",
                "route" => "https://google.com",
                "rank" => 1,
                "is_active" => 1,
                "parent_id" => 6
            ]
        ];

        DB::table("footer_items")->insert($itemData);

        \Illuminate\Support\Facades\Schema::enableForeignKeyConstraints();
    }
}
