<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = DB::table("options");

        $table->truncate();

        $options = [
            "site_title" => "STUKET",
            "logo" => null,
            "favicon" => null,
            "gsm" => "+90 537 051 31 07",
            "contact_email" => "hi@stuket.com",
            "info_email" => "info@stuket.com",
            "address" => "Selçuk, İzmir",
            "google_map" => null,
            "site_key" => null,
            "secret_key" => null
        ];

        $table->insert($options);
    }
}
