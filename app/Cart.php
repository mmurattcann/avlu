<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class Cart extends Model
{
    protected $guarded = [ "id" ];

    public function owner(){
        return $this->belongsTo(User::class);
    }


    public function setIpAttribute($value){

        return $this->attributes["ip"] = Request::ip();

    }
}
