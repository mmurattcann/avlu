<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Page extends Model
{
    protected $guarded = ['id'];

    public function getShortenDescriptionAttribute(){
        $attr = $this->attributes["description"];
        return  Str::limit($attr,5,"...");
    }
}
