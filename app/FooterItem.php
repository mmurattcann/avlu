<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FooterItem extends Model
{
    protected $fillable = ["title", "slug", "description", "rank", "is_active", "route", "parent_id"];

    public function parent(){
        return $this->belongsTo(FooterColumn::class, "parent_id");
    }
}
