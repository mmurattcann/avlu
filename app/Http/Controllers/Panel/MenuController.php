<?php

namespace App\Http\Controllers\Panel;

use App\Enums\ProductEnums;
use App\Http\Controllers\Controller;
use App\Repositories\Classes\MenuRepository;
use App\Traits\GeneralCrud;
use App\Traits\StatusUpdater;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    use GeneralCrud, StatusUpdater;

    private  $repository = null;
    private  $resource   = "menus";
    private  $enum = null;

    public function __construct()
    {
        $this->repository = new MenuRepository();
        $this->enum = new ProductEnums();
    }

    public function index()
    {
        $menus = $this->repository->getAll();

        $data = [
            "title" => "Menü Yönetimi",
            "menus" => $menus,
            "isActive"  => $this->enum::_ACTIVE_PRODUCT
        ];

        return panelView("$this->resource.index")->with($data);
    }

    public function create(){

        $menus = $this->repository->getAllParents();
        $data = [
            "title" => "Yeni Menü Ekle",
            "menus" => $menus
        ];

        return panelView("$this->resource.create")->with($data);
    }

    public function edit($id){

        $menu = $this->repository->getById($id);

        $data = [
            "title" => $menu->title. " Menüsü Düzenleniyor",
            "menu" => $menu,
            "parents" => $this->repository->getAllParents()
        ];

        return panelView("$this->resource.edit")->with($data);
    }
}
