<?php

namespace App\Http\Controllers\Panel;

use App\Enums\ProductEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\Repositories\Classes\FooterItemRepository;
use App\Repositories\Classes\FooterRepository;
use App\Traits\StatusUpdater;
use Illuminate\Http\Request;

class FooterItemController extends Controller
{
    use StatusUpdater;

    private  $repository = null;
    private  $resource   = "footer-items";
    private  $enum = null;

    public function __construct()
    {
        $this->repository = new FooterItemRepository();
        $this->enum = new ProductEnums();
    }

    public function index($parentId)
    {


        $items = $this->repository->getAllByParent($parentId);

        $data = [
            "title" => "Footer Satır Yönetimi",
            "items" => $items,
            "isActive"  => $this->enum::_ACTIVE_PRODUCT,
            "parentID" => $parentId
        ];

        return panelView("$this->resource.index")->with($data);
    }

    public function  show($id){
        dd($id);
    }
    public function create($parentID){


        $data = [
            "title" => "Yeni Satır Ekle",
            "parentID" => $parentID
        ];

        return panelView("$this->resource.create")->with($data);
    }

    public function store(Request  $request){
        $parentID = $request->parent_id;
        $this->repository->store($request);

        return RedirectHelper::RedirectWithSuccessFlashMessage("store", "itemIndex", ["parentId" => $parentID]);
    }

    public function edit($id){

        $item = $this->repository->getItemById($id);

        $data = [
            "title" => $item->title. " Satırı Düzenleniyor",
            "item" => $item,
        ];

        return panelView("$this->resource.edit")->with($data);
    }

    public function update(Request $request, $id){

        $parentID = $request->parent_id;
        $this->repository->update($request, $id);

        return RedirectHelper::RedirectWithSuccessFlashMessage("update", "itemIndex", [ "parentId" => $parentID]);

    }

    public function destroy($id){
        $this->repository->destroyItem($id);
        return response()->json(["message" => "Kayıt Başarıyla Silindi"], 200);
    }
}
