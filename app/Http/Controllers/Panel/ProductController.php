<?php

namespace App\Http\Controllers\Panel;

use App\Enums\ProductEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\Repositories\Classes\FacultyRepository;
use App\Repositories\Classes\ProductCategoryRepository;
use App\Repositories\Classes\ProductImageRepository;
use App\Repositories\Classes\ProductRepository;
use App\Repositories\Classes\UniversityRepository;
use App\Traits\GeneralCrud;
use App\Traits\StatusUpdater;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use GeneralCrud, StatusUpdater;
    private $repository         = null;
    private $productImageRepository  = null;
    private $universityRepository    = null;
    private $facultyRepository       = null;
    private $categoryRepository = null;
    private $productEnums       = null;
    private $resource           = "products";
    public function __construct()
    {
        $this->repository         = new ProductRepository();
        $this->productImageRepository  = new ProductImageRepository();
        $this->categoryRepository = new ProductCategoryRepository();
        $this->productEnums       = new ProductEnums();
    }

    public function index()
    {

        $data = [
            "title" => "Ürün Yönetimi",
            "products" => $this->repository->getAll(),
            "isActive"   => $this->productEnums::_ACTIVE_PRODUCT
        ];

        return panelView("$this->resource.index")->with($data);
    }

    public function create()
    {
        $data = [
            "title" => "Yeni Ürün Kaydı",
            "categories" => $this->categoryRepository->getAll("title", "asc")
        ];

        return panelView("$this->resource.create")->with($data);
    }

    public function storeProductImage(Request $request){
       $this->repository->storeProductImage($request);
       return response()->json(["Message" => "Kayıt Başarıyla Eklendi"]);
    }

    public function getImages($productID){
        $images = $this->productImageRepository->getAllByProductId($productID);


        return panelView("$this->resource.product-images")->with(["images" => $images,  "productID" => $productID]);
    }
    public function  storeProduct(Request $request){

        $product = $this->repository->store($request);

        return panelView("$this->resource.create-image")->with(["product" => $product]);
    }

    public function  updateImageStatus(Request $request){
        $this->repository->updateImageStatus($request);
        return response()->json(["message" => "Resim Durumu Güncellendi" ], 200);

    }

    public function  setCover(Request $request){
        $this->productImageRepository->setCover($request);
        return response()->json(["message" => "Görsel, kapak resmi olarak ayarlandı" ], 200);

    }

    public function forwardProduct(Request $request){

        $this->repository->setForward($request);
        return response()->json(["message" => "Ürün durumu güncellendi" ], 200);

    }

    public function edit($id)
    {
        $product = $this->repository->getById($id);

        $data = [
            "title" => $product->title. " başlıklı ürün düzenleniyor",
            "product" => $product,
            "categories" => $this->categoryRepository->getAll(),
        ];
        return  panelView("$this->resource.edit")->with($data);
    }

}
