<?php

namespace App\Http\Controllers\Panel;

use App\Enums\BlogEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;

use App\Repositories\Classes\BoxRepository;
use App\Traits\GeneralCrud;
use App\Traits\StatusUpdater;
use Illuminate\Http\Request;

class BoxController extends Controller
{
    use GeneralCrud, StatusUpdater;

    private $repository = null;
    private $resource   = "boxes";
    private $enum =  null;

    public function __construct()
    {
        $this->repository = new BoxRepository();
        $this->enum = new BlogEnums();
    }

    public function index(){

        $boxes = $this->repository->getAll();

        $data = [
            "title" => "Kutu Yönetimi",
            "boxes" => $boxes,
            "isActive" => $this->enum::_ACTIVE,
        ];

        return panelView("$this->resource.index")->with($data);
    }

    public function create(){
        $title = "Yeni Kutu Ekle";
        $normalButtonClasses = config("buttonClasses.normalButtons");
        $outlinedButtonClasses = config("buttonClasses.outlineButtons");

        $data = [
            "title" => $title,
            "normalButtonClasses" => $normalButtonClasses,
            "outlinedButtonClasses" => $outlinedButtonClasses,
        ];

        return panelView("$this->resource.create")->with($data);
    }


    public function edit($id){

        $box = $this->repository->getById($id);

        $data = [
            "title" => $box->title." başlıklı kutu düzenleniyor",
            "box" => $box,
        ];

        return panelView("$this->resource.edit")->with($data);
    }

}
