<?php

namespace App\Http\Controllers\Panel;

use App\Enums\UserEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserCreateRequest;
use App\Repositories\Interfaces\IUserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $repository = null;
    private $userEnum   = null;
    private $resource   = "users";

    public function __construct(IUserRepository $userRepository)
    {
        $this->repository = $userRepository;
        $this->userEnum = new UserEnums();
    }

    public function index()
    {
        $users = $this->repository->getAll(["created_at","desc"]);

        $data = [
            "title" => "Kullanıcı Yönetimi",
            "users" => $users,
            "isActive"  => $this->userEnum::_ACTIVE_USER,
        ];

        return panelView("$this->resource.index")->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            "title" => "Yeni Kullanıcı Ekle",
            "adminEnum" => $this->userEnum::_ADMIN_USER,
            "standardUserEnum" => $this->userEnum::_STANDARD_USER,
        ];

        return panelView("$this->resource.create")->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        $this->repository->store($request);
        return RedirectHelper::RedirectWithSuccessFlashMessage("store", "$this->resource.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->repository->getById($id);
        $data = [
            "title" => "Kullanıcı Düzenle",
            "user"  => $user
        ];
        return panelView("$this->resource.edit")->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->repository->update($id, $request);
        return RedirectHelper::RedirectWithSuccessFlashMessage("store","$this->resource.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updateUserStatus(Request $request){

        $this->repository->updateUserStatus($request);
        return response()->json(["message" => "Kullanıcı durumu güncellendi"]);
    }
    public function destroy($id)
    {
       $this->repository->delete($id);

       return response()->json(["message" => "Kayıt başarıyla silindi."]);
    }
}
