<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Repositories\Classes\BlogRepository;
use App\Repositories\Classes\ProductRepository;
use App\Repositories\Classes\UniversityRepository;
use App\Repositories\Classes\UserRepository;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    private $productRepository = null;
    private $userRepository = null;
    private $blogRepository = null;
    private $universityRepository = null;

    public function __construct()
    {
        $this->productRepository = new ProductRepository();
        $this->userRepository    = new UserRepository();
        $this->blogRepository    = new BlogRepository();
    }

    public function index(){

        $lastProducts = $this->productRepository->baseQuery()->take(5)->latest()->get();
        $lastUsers    = $this->userRepository->baseQuery()->take(5)->latest()->get();

        $totalProducts = $this->productRepository->baseQuery()->count();
        $totalBlogs    = $this->blogRepository->baseQuery()->count();
        $totalUsers    = $this->userRepository->baseQuery()->count();

        $data = [
            "title" => "Yönetim Ana Sayfa| İstatistikler",
            "lastProducts" => $lastProducts,
            "lastUsers" => $lastUsers,
            "blogCount" =>  $totalBlogs,
            "userCount" => $totalUsers,
            "productCount" => $totalProducts
        ];
        return panelView("dashboard.index")->with($data);
    }
}
