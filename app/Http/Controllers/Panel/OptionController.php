<?php

namespace App\Http\Controllers\Panel;

use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\Repositories\Classes\OptionRepository;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    public $repository = null;
    public $resource   = "options";

    public function __construct()
    {
        $this->repository = new OptionRepository();
    }

    public function edit()
    {
        $option = $this->repository->getFirst();

        $data = [
            "title"   => "Site Ayarları",
            "option" => $option,
            "route"   => route("$this->resource.update", ["id" => $option->id])
        ];

        return panelView("$this->resource.edit")->with($data);

    }
    public function update(Request $request, $id)
    {
        $this->repository->update($request, $id);
        return RedirectHelper::RedirectWithSuccessFlashMessage("update","options.edit");
    }
}
