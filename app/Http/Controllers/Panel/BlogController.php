<?php

namespace App\Http\Controllers\Panel;

use App\Enums\BlogEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\BlogStoreRequest;
use App\Http\Requests\Blog\BlogUpdateRequest;
use App\Repositories\Classes\BlogCategoryRepository;
use App\Repositories\Classes\BlogRepository;
use App\Traits\GeneralCrud;
use App\Traits\StatusUpdater;
use Illuminate\Http\Request;
use Mmurattcann\LaraImage\LaraImage;

class BlogController extends Controller
{
    use GeneralCrud, StatusUpdater;

    private $repository = null;
    private $categoryRepository = null;
    private $enum   = null;
    private $resource   = null;
    private $uploadPath = "uploads/blogs";
    public function __construct()
    {
        $this->repository = new BlogRepository();
        $this->categoryRepository = new BlogCategoryRepository();
        $this->enum   = new BlogEnums();
        $this->resource   = "blogs";
    }
    public function index()
    {
        $data = [
            "title"    => "Blog Yönetimi",
            "blogs"    => $this->repository->getAll(),
            "isActive" => $this->enum::_ACTIVE,
        ] ;

        return panelView("$this->resource.index")->with($data);
    }


    public function create()
    {
        $data = [
            "title" => "Yeni Sayfa Oluştur",
            "categories" => $this->categoryRepository->getAll("title", "asc")
        ];

        return panelView("$this->resource.create")->with($data);
    }

    public function edit($id)
    {
        $blog = $this->repository->getById($id);

        $data = [
            "title" => $blog->title .  " Blogu Düzenleniyor",
            "blog"  => $blog,
            "categories" => $this->categoryRepository->getAll("title","asc")
        ];

        return panelView("$this->resource.edit")->with($data);
    }

}
