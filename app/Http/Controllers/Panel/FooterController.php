<?php

namespace App\Http\Controllers\Panel;

use App\Enums\ProductEnums;
use App\Http\Controllers\Controller;
use App\Repositories\Classes\FooterRepository;
use App\Traits\GeneralCrud;
use App\Traits\StatusUpdater;
use Illuminate\Http\Request;

class FooterController extends Controller
{
    use GeneralCrud,StatusUpdater;

    private  $repository = null;
    private  $resource   = "footer-columns";
    private  $enum = null;

    public function __construct()
    {
        $this->repository = new FooterRepository();
        $this->enum = new ProductEnums();
    }

    public function index()
    {
        $columns = $this->repository->getAllColumn();

        $data = [
            "title" => "Footer Yönetimi",
            "columns" => $columns,
            "isActive"  => $this->enum::_ACTIVE_PRODUCT
        ];

        return panelView("$this->resource.index")->with($data);
    }

    public function create(){

        $data = [
            "title" => "Yeni Kolon Ekle",
        ];

        return panelView("$this->resource.create")->with($data);
    }

    public function edit($id){

        $column = $this->repository->getColumnById($id);

        $data = [
            "title" => $column->title. " Kolonu Düzenleniyor",
            "column" => $column,
        ];

        return panelView("$this->resource.edit")->with($data);
    }
}
