<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repositories\Classes\BlogRepository;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    private $repository;
    private $resource = "blogs";

    public function __construct()
    {
        $this->repository = new BlogRepository();
    }


    public function index(){

        $data = [
            "title" => "Bloglarımız",
            "blogs" => $this->repository->listingMap($this->repository->getAllActive())
        ];

        return frontView("$this->resource.index")->with($data);
    }

    public function detail($slug){
        $blog = $this->repository->getBySlug($slug);


        $data = [
            "title" => $blog->title,
            "blog" => $blog,
            "similars" => $this->repository->listingMap($this->repository->getSimilars($blog->id))
        ];

        return frontView("$this->resource.detail")->with($data);
    }
}
