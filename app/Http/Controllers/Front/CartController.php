<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repositories\Classes\CartRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new CartRepository();
    }

    public function add(Request $request){

        if(Auth::check()){
            $this->repository->addChart($request);
            return response()->json( ["message" => "Ürün Sepete Eklendi", "status" => 201], 201);

        }else{
            return response()->json( ["message" => "Lütfen Önce Giriş Yapın"], 401);
        }
    }
}
