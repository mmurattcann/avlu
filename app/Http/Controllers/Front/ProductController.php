<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repositories\Classes\ProductCategoryRepository;
use App\Repositories\Classes\ProductRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $repository;
    private $categoryRepository;
    private $resource = "products";

    public function __construct()
    {
        $this->categoryRepository = new ProductCategoryRepository();
        $this->repository = new ProductRepository();
    }


    public function index(){


        $data = [
            "title" => "Ürünlerimiz",
            "products" => $this->repository->listingMap($this->repository->getAllActive()),
            "categories" => $this->categoryRepository->listingMap($this->categoryRepository->getActiveParents()),
            "maxPrice" => $this->repository->getMaximumPrice(),
            "minPrice" => $this->repository->getMinimumPrice()

        ];

        return frontView("$this->resource.index")->with($data);
    }

    public function detail($slug){
        $product = $this->repository->getBySlug($slug);


        $data = [
            "title" => $product->title,
            "product" => $this->repository->detailMap($product),
            "similars" => $this->repository->listingMap($this->repository->getSimilars($product->id))
        ];

        return frontView("$this->resource.detail")->with($data);
    }
}
