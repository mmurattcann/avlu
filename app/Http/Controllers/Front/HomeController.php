<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repositories\Classes\BlogRepository;
use App\Repositories\Classes\BoxRepository;
use App\Repositories\Classes\ProductRepository;
use App\Repositories\Classes\SliderRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    protected $sliderRepository;
    protected $blogRepository;
    protected $boxRepository;
    protected $productRepository;

    public function __construct()
    {
        $this->sliderRepository = new SliderRepository();
        $this->blogRepository   = new BlogRepository();
        $this->boxRepository    = new BoxRepository();
        $this->productRepository = new ProductRepository();

    }

    public function index(){

        $data = [
            "title" => "Anasayfa",
            "sliders" => $this->sliderRepository->getAllActive(),
            "blogs" => $this->blogRepository->listingMap($this->blogRepository->getIndexBlogs()),
            "boxes" => $this->boxRepository->listingMap($this->boxRepository->getAllActive()),
            "products" => $this->productRepository->listingMap($this->productRepository->getIndexProducts()),
            "featuredProducts" => $this->productRepository->listingMap($this->productRepository->getFeaturedProducts())
        ];

        return frontView("index")->with($data);
    }
}
