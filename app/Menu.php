<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

    protected $fillable = ["title", "slug", "description", "rank", "is_active", "route", "parent_id"];

    public function children(){
        return $this->hasMany(Menu::class,"parent_id");
    }

    public function parent(){
        return $this->belongsTo(Menu::class,"parent_id")->where("parent_id", null)->with("parent");
    }

    public function scopeParent($query){
        return $query->where("parent_id", null)->orWhere("parent_id", 0);
    }

    public function scopeChild($query){
        return $query->whereNotNull("parent_id");
    }

    public function scopeActive($query){
        return $query->where("is_active", 1);
    }

    public function getTypeAttribute($value){
        if($this->parent_id == null or $this->parent_id == 0)
            return "Ana Menü";

        return "Alt Menü";
    }


}
