<?php

namespace App\Providers;

use App\Repositories\Classes\BlogCategoryRepository;
use App\Repositories\Classes\FooterRepository;
use App\Repositories\Classes\MenuRepository;
use App\Repositories\Classes\ProductCategoryRepository;
use App\Repositories\Classes\OptionRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Schema::defaultStringLength(191);

        $menuRepository = new MenuRepository();
        $footerRepository = new FooterRepository();

        if(!$this->app->runningInConsole()){

            $optionRepository = new  OptionRepository();

            $options = Cache::remember('options', 60, function() use ($optionRepository)
            {
                return $optionRepository->detailMap($optionRepository->getFirst());
            });

            config()->set('options', $options);


            $menus = Cache::remember('menus', 60, function() use ($menuRepository)
            {
                return $menuRepository->listingMap($menuRepository->getAllParents());
            });

            config()->set('menus', $menus);

            $footers = Cache::remember('footers', 60, function() use ($footerRepository)
            {
                return $footerRepository->listingMap($footerRepository->getAllColumn());
            });

            config()->set('footers', $footers);

        }

            $productCategoryRepository = new ProductCategoryRepository();
            $blogCategoryRepository = new BlogCategoryRepository();
            $optionRepository = new OptionRepository();




            $data = [
                "productCategories" => $productCategoryRepository->getAllParents(),
                "menus" => config("menus"),
                "footers" => config("footers"),
                "blogCategories"    => $blogCategoryRepository->getAll(),
                "siteTitle" => config("options.site_title"),
                "siteAddress" => config("options.address"),
                "mobilePhoneNumber" => config("options.gsm"),
                "phoneNumber" => config("options.phone"),
                "contactEmail" => config("options.contact_email"),
                "infoEmail" => config("options.info_email"),
                "siteLogo" => config("options.logo"),
                "favicon" => config("options.favicon"),
                "siteKey" => config("options.site_key"),
                "secretKey" => config("options.secret_key"),
                "googleMap" => config("options.google_map")
            ];



            return View::share($data);
    }
}
