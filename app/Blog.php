<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class Blog extends Model
{
    protected $guarded = ["id"];
    protected $fillable = ["title", "slug", "image", "description", "content", "is_active", "user_id", "category_id"];

    public function category(){
        return $this->belongsTo(BlogCategory::class, "category_id");
    }

    public function comments(){
        return $this->hasMany(BlogComment::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function getShortenDescriptionAttribute(){

        return Str::limit($this->content, 70, "...");

    }

    public function getModifiedDateAttribute($created_at){

        $carbon = new Carbon();
        $carbon->locale("tr");
        return $carbon->diffForHumans($this->attributes["created_at"]);
    }

    public function getShortenContentAttribute(){
        return Str::limit(strip_tags($this->content), 444, "...");
    }
}
