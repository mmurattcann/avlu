<?php


namespace App\Traits;


use Illuminate\Http\Request;

trait StatusUpdater
{

    public function updateStatus(Request $request){
        $this->repository->updateStatus($request);
        return response()->json(["message" => "Kayıt Durumu Güncellendi" ], 200);
    }
}
