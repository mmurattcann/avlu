<?php


namespace App\Repositories\Interfaces;


use Illuminate\Http\Request;

interface IUserRepository
{
    public function baseQuery();

    public function getById(Int $id);

    public function getAll($orderBy = ["id", "asc"]);

    public function store(Request $request);

    public function update($id, Request $request);

    public function delete($id);

    public function updateUserStatus(Request $request);

}
