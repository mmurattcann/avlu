<?php
namespace App\Repositories\Classes;

use App\Blog;
use App\Enums\BlogEnums;
use App\Http\Requests\Blog\BlogStoreRequest;
use App\Http\Requests\Blog\BlogUpdateRequest;
use App\Repositories\Interfaces\IBaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Murattcann\LaraImage\LaraImage;

setlocale(LC_TIME,"Turkish");
class BlogRepository implements IBaseRepository
{
    protected $model;
    protected $enum;
    protected $uploadPath = "uploads/blogs";
    protected $relations  = null;
    protected $imageWidth  = null;
    protected $imageHeight = null;

    public function __construct()
    {
        $this->model = new Blog();
        $this->enum = new BlogEnums();
        $this->relations= ["category", "user"];

        $this->imageWidth   = config("imageUpload.blog.cover.width");
        $this->imageHeight  = config("imageUpload.blog.cover.height");
    }

    public function baseQuery()
    {
        return $this->model::query()->with($this->relations);
    }

    public function getById(int $id)
    {
        return $this->baseQuery()->find($id);
    }

    public function getBySlug(string $slug)
    {
        return $this->baseQuery()->where('slug', $slug)->first();
    }

    public function getAll(string $order = "id", string $by = "asc")
    {
        return $this->baseQuery()->orderBy($order, $by)->get();
    }

    public function getAllWithPaginate(int $limit = 10)
    {

        return $this->baseQuery()->paginate($limit);
    }

    public function getAllWithWhere( string $where = null, string $operator = "=", $condition = null)
    {

        return $this->baseQuery()->where($where, $operator, $condition)->get();
    }

    public function getAllActive(){
        return $this->baseQuery()->where("is_active","=", "1")->get();
    }

    public function getIndexBlogs(){
        return $this->baseQuery()->where("is_active", 1)->take(6)->get();
    }

    /*
     * if you change a spesific value of request key
     * For Example; if you want to modify file request named image
     * You can use like that:
     *
     * $data["image"] = Mmurattcann/LaraImage::upload($request->file('image'))
    */
    public function store(Request $request)
    {

        $data = $request->only($this->model->getFillable());

        $data["image"] = LaraImage::upload("store", "/uploads/blogs",$data["title"],$data["image"],$this->imageWidth, $this->imageHeight);

        $slug = $data["slug"];

        if($slug == null or $slug == "")
            $data["slug"] = Str::slug($data["title"], "-");

        return  $this->baseQuery()->create($data);
    }

    /*
     * You can also modify this request
     *  For Example;
     *
     *  $data["name"] = Str::upper("john doe");
     */
    public function update(Request $request, int $id)
    {

        $model = $this->getById($id);

        $data = $request->all();

        if($request->hasFile("image")){

            $image = $request->file("image");
            $data["image"] = LaraImage::upload("update",$this->uploadPath,$data["title"], $image, $this->imageWidth, $this->imageHeight);

        }
        return $model->update($data);
    }

    public function updateStatus(Request $request)
    {
        $id = $request->id;

        $blog = $this->getById($id);

        $active = $this->enum::_ACTIVE;

        $inActive = $this->enum::_INACTIVE;

        $is_active = $request->get("is_active") == $active ? $active : $inActive;

        $blog->is_active = $is_active;

        return $blog->save();
    }

    public function destroy(int $id)
    {

        $model = $this->getById($id);

        return $model->delete();
    }

    public function getSimilars($id){
        return $this->baseQuery()->where("id", "!="," $id")->where("is_active", "=", "1")->take(3)->get();
    }

    public function listingMap($collect){
        return $collect->map(function ($item){


            return [
                "id" => $item->id,
                "title" => $item->title,
                "slug"  => $item->slug,
                "seo_description" => $item->description,
                "image" => getImage( "blogs", $item->image),
                "content" => $item->content,
                "short_description" => strip_tags($item->shorten_description),
               // "categoryName" => $item->category->title,
                //"user" => $item->user->name,
                "created_at" => turkishDate($item->created_at),
               "route" => route("front.blog-detail", ["slug" => $item->slug])
            ];
        });
    }
}
