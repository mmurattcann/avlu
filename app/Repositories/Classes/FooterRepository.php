<?php


namespace App\Repositories\Classes;


use App\Enums\CategoryEnums;
use App\FooterColumn;
use App\FooterItem;
use App\Helpers\RedirectHelper\RedirectHelper;

use App\Repositories\Interfaces\IBaseRepository;
use Illuminate\Http\Request;


class FooterRepository
{
    private $column  = null;
    private $footerItem  = null;
    protected $itemRepository = null;
    private $enum  = null;
    private $columnRelations = null;
    private $itemRelations = null;

    public function __construct()
    {
        $this->column = new FooterColumn();
        $this->footerItem = new FooterItem();
        $this->itemRepository = new FooterItemRepository();
        $this->enum = new CategoryEnums();
        $this->columnRelations = ["children"];
        $this->itemRelations = ["parent"];
    }

    public function columnBaseQuery()
    {
        return $this->column::query();
    }

    public function itemBaseQuery()
    {
        return $this->footerItem::query()->with($this->itemRelations);
    }

    public function getColumnById(Int $id)
    {
        return  $this->columnBaseQuery()->find($id);
    }

    public function getColumnBySlug(string $slug)
    {
        return $this->columnBaseQuery()->where("slug", $slug)->first();
    }


    public function getAllColumn(string $order = "id", string $by = "desc")
    {
        return $this->columnBaseQuery()->orderBy($order, $by)->get();
    }
    public function getAllActiveColumn()
    {

        return $this->columnBaseQuery()->where("is_active", 1)->orderBy("rank", "asc")->get();
    }

    public function getAllChilds(string $order= "id", string $by= "desc")
    {
        return $this->columnBaseQuery()->children()->orderBy($order, $by)->get();
    }
    public function store(Request $request)
    {


        $data = $request->only($this->column->getFillable());

        return $this->column->create($data);
    }

    public function update(Request $request, $id)
    {


        $column = $this->getColumnById($id);

        $isActive = $column->is_active == $this->enum::_ACTIVE_CATEGORY ? $this->enum::_ACTIVE_CATEGORY : $this->enum::_INACTIVE_CATEGORY;

        $data = $request->except("categoryRadio");

        if($request->hasFile("image")){

            $image = $request->file("image");
            $data["is_active"] = $isActive;

        }

        $column->update($data);
    }
    public function updateStatus(Request $request)
    {
        $id = $request->id;

        $column = $this->getColumnById($id);

        $active = $this->enum::_ACTIVE_CATEGORY;

        $inActive = $this->enum::_INACTIVE_CATEGORY;

        $is_active = $request->get("is_active") == $active ? $active : $inActive;

        $column->is_active = $is_active;

        return $column->save();
    }

    public function destroy(int $id)
    {
        $column = $this->getColumnById($id);



        $column->delete();

        RedirectHelper::RedirectWithSuccessFlashMessage("destroy","menus.index");
    }

    public function listingMap($collect){

        return $collect->map(function ($item){
            return [
                "id" => $item->id,
                "title" => $item->title,
                "children" => $this->itemRepository->listingMap($item->children),
            ];
        });
    }
}
