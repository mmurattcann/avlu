<?php


namespace App\Repositories\Classes;


use App\Enums\CategoryEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\ProductCategory;
use App\Repositories\Interfaces\IBaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Murattcann\LaraImage\LaraImage;

class ProductCategoryRepository implements IBaseRepository
{
    private $category  = null;
    private $categoryEnum  = null;
    private $relations     = null;
    private $uploadPath    = "uploads/product-categories";
    public function __construct()
    {
        $this->category = new ProductCategory();
        $this->categoryEnum = new CategoryEnums();
        $this->relations = ["childs","parent", "products"];
    }

    public function baseQuery()
    {
        return $this->category::query()->with($this->relations);
    }

    public function getById(Int $id)
    {
        return  $this->baseQuery()->find($id);
    }

    public function getBySlug(string $slug)
    {
        return $this->baseQuery()->where("slug", $slug)->first();
    }


    public function getAll(string $order = "id", string $by = "desc")
    {
        return $this->baseQuery()->orderBy($order, $by)->get();
    }
    public function getAllActive()
    {

        return $this->baseQuery()->active()->orderBy("title", "asc")->get();
    }

    public function getAllParents(string $order= "id", string $by= "desc")
    {
        return $this->baseQuery()->parent()->orderBy($order, $by)->get();
    }

    public function getActiveParents(){
        return $this->baseQuery()
            ->where("is_active", "=","1")
            ->where("parent_id", "=", null)
            ->orWhere("parent_id" ,"=", "0")->get();
    }
    public function getAllChilds(string $order= "id", string $by= "desc")
    {
        return $this->baseQuery()->child()->orderBy($order, $by)->get();
    }
    public function store(Request $request)
    {
        $image = $request->file("image");

        $data = $request->except("categoryRadio");

        $data["image"] = LaraImage::upload("store",$this->uploadPath,$request->get("title"),$image,500,500);

        return $this->category->create($data);
    }

    public function update(Request $request, $id)
    {


        $category = $this->getById($id);

        $isActive = $category->is_active == $this->categoryEnum::_ACTIVE_CATEGORY ? $this->categoryEnum::_ACTIVE_CATEGORY : $this->categoryEnum::_INACTIVE_CATEGORY;

        $data = $request->except("categoryRadio");

        if($request->hasFile("image")){

            $image = $request->file("image");
            $data["is_active"] = $isActive;
            $data["image"] = LaraImage::upload("update",$this->uploadPath,$data["title"], $image, 500, 500);

        }

        $category->update($data);
    }
    public function updateStatus(Request $request)
    {
        $id = $request->id;

        $category = $this->getById($id);

        $active = $this->categoryEnum::_ACTIVE_CATEGORY;

        $inActive = $this->categoryEnum::_INACTIVE_CATEGORY;

        $is_active = $request->get("is_active") == $active ? $active : $inActive;

        $category->is_active = $is_active;

        return $category->save();
    }

    public function destroy(int $id)
    {
        $category = $this->getById($id);

        LaraImage::deleteUploadedFile($this->uploadPath, $category->image);

        $category->delete();

        RedirectHelper::RedirectWithSuccessFlashMessage("destroy","product-categories.index");
    }

    public function listingMap($collect){
        return $collect->map(function($item){
            return [
                "title" => $item->title,
                "slug" => $item->slug,
                "childCount" => $item->childs()->count(),
                "children" => $item->childs,
                "route" => "#"
            ];
        });
    }
}
