<?php
namespace App\Repositories\Classes;


use App\Box;
use App\Enums\BlogEnums;
use App\Repositories\Interfaces\IBaseRepository;
use Illuminate\Http\Request;
use Murattcann\LaraImage\LaraImage;

class BoxRepository implements IBaseRepository
{
    protected $model;
    protected $enum;
    protected $uploadPath = "uploads/boxes";
    protected $relations  = null;
    protected $imageWidth  = null;
    protected $imageHeight = null;

    public function __construct()
    {
        $this->model = new Box();

        $this->imageWidth   = config("imageUpload.box.width");
        $this->imageHeight  = config("imageUpload.box.height");
        $this->enum = new BlogEnums();
    }

    public function baseQuery()
    {
        return $this->model::query();
    }

    public function getById(int $id)
    {
        return $this->baseQuery()->find($id);
    }

    public function getBySlug(string $slug)
    {
        return $this->baseQuery()->where('slug', $slug)->first();
    }

    public function getAll(string $order = "id", string $by = "desc")
    {
        return $this->baseQuery()->orderBy($order, $by)->get();
    }

    public function getAllActive(string $order = "rank", string $by = "asc")
    {
        return $this->baseQuery()->where("is_active", "=", 1)->orderBy($order, $by)->get();
    }

    /*
     * if you change a spesific value of request key
     * For Example; if you want to modify file request named image
     * You can use like that:
     *
     * $data["image"] = Murattcann/LaraImage::upload($request->file('image'))
    */
    public function store(Request $request)
    {

        $data = $request->only($this->model->getFillable());

        $data["image"] = LaraImage::upload("store", $this->uploadPath,$data["title"],$data["image"],$this->imageWidth, $this->imageHeight);

        return  $this->baseQuery()->create($data);
    }

    /*
     * You can also modify this request
     *  For Example;
     *
     *  $data["name"] = Str::upper("john doe");
     */
    public function update(Request $request, int $id)
    {

        $model = $this->getById($id);

        $data = $request->only($this->model->getFillable());

        if($request->hasFile("image")){
            $image = $request->file("image");
            $data["image"] = LaraImage::upload("update",$this->uploadPath,$data["title"], $image, $this->imageWidth, $this->imageHeight);
        }

        return $model->update($data);
    }

    public function updateStatus(Request $request)
    {
        $id = $request->id;

        $slider = $this->getById($id);

        $active = $this->enum::_ACTIVE;

        $inActive = $this->enum::_INACTIVE;

        $is_active = $request->get("is_active") == $active ? $active : $inActive;

        $slider->is_active = $is_active;

        return $slider->save();
    }

    public function destroy(int $id)
    {

        $box = $this->getById($id);
        LaraImage::deleteUploadedFile($this->uploadPath, $box->image);
        return $box->delete();
    }

    public function listingMap($collect){
        return $collect->map(function ($item){
           return [
                "id" => $item->id,
                "title" => $item->title,
                "description" => $item->description,
                "image" => getImage("boxes", $item->image),
                "buttonText" => $item->button_text,
                "route" => $item->route
            ];
        });
    }
}
