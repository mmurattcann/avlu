<?php
namespace App\Repositories\Classes;

use App\Cart;
use App\CartProduct;
use App\Enums\PageEnums;
use App\Repositories\Interfaces\IBaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CartRepository
{
    protected $cartModel;
    protected $itemModel;
    protected $enum;
    protected $userId;
    protected $productRepository;

    public function __construct()
    {

        $this->cartModel = new Cart();
        $this->itemModel = new CartProduct();
        $this->enum  = new PageEnums();

        $this->productRepository = new ProductRepository();


    }

    public function userId(){
        return Auth::user()->id;
    }
    public function baseQuery()
    {
        return $this->cartModel::query()->with(["owner"]);
    }

    public function itemBaseQuery(){
        return $this->itemModel::query()->with(["cart", "product"]);
    }

    public function getById(int $id)
    {
        return $this->baseQuery()->find($id);
    }

    public function getAll(string $order = "id", string $by = "asc")
    {
        return $this->baseQuery()->orderBy($order, $by)->get();
    }

    public function getAllWithWhere( string $where = null , string $condition = null)
    {

        return $this->baseQuery()->where($where, $condition)->get();
    }


    public function destroy(int $id)
    {

        $cartModel = $this->getById($id);

        return $cartModel->delete();
    }



    public function checkCartExists(){

        $userID = Auth::user()->id;

        return $this->baseQuery()->where("user_id", "=", "$userID")->where("status", "=", "0")->first();
    }

    public function checkProduct(Request $request){

        $userId = Auth::user()->id;

        return $this->itemBaseQuery()
            ->where("user_id", "=", "$userId")
            ->where("status", "=", 0)
            ->where("product_id", "=", "$request->product_id")->first();
    }

    public function getItemByProductID($productId){

        return $this->itemBaseQuery()->where("user_id", "=", "{$this->userId()}")
            ->where("status", "=", "0")
            ->where("product_id", "=", "$productId")->first();
    }

    public function getItemByCartAndProduct($cartID, $productId){
        return $this->itemBaseQuery()
            ->where("cart_id","=", "$cartID")
            ->where("status", "=", "0")
            ->where("product_id", "=", "$productId")->first();
    }
    public function increaseCount($productId){

        $item = $this->getItemByProductID($productId);

        $quantity = $item->quantity;

        $quantity++;

        $item->quantity = $quantity;

        $item->save();

        return $this->getItemByProductID($productId);
    }
    public function create(){
        $data = [
            "ip" => \Illuminate\Support\Facades\Request::ip(),
            "user_id" => Auth::user()->id,
            "status" => 0,
            "is_active" => 1
        ];

        return $this->baseQuery()->create($data);
    }

    public function addItem($cartId, $productId, $quantity = 1){

        $product = $this->productRepository->getById($productId);

        $data = [
            "price" => $product->price,
            "quantity" => $quantity ,
            "status" => 0,
            "product_id" => $productId,
            "cart_id" => $cartId,
            "user_id" => $this->userId()
        ];

        return $this->itemBaseQuery()->create($data);
    }

    public function addChart(Request $request){


        $quantity = $request->quantity != null ?  $request->quantity : 1;

        $productId = $request->productId;

        $cart = $this->checkCartExists();

        if(!$cart){
           $cart =  $this->create();
        }

        $cartItem = $this->getItemByCartAndProduct($cart->id, $productId);

        if($cartItem){
            return  $this->increaseCount($productId);
        }
        else {
            return $this->addItem($cart->id, $productId, $quantity);
        }
    }



}
