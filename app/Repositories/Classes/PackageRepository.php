<?php


namespace App\Repositories\Classes;


use App\Enums\CategoryEnums;
use App\Enums\ProductEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Package;
use App\Product;
use App\ProductCategory;
use App\ProductImage;
use App\Repositories\Interfaces\IBaseRepository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Murattcann\LaraImage\LaraImage;

class PackageRepository implements IBaseRepository
{
    protected $package       = null;
    protected $productEnum   = null;
    protected $relations     = null;
    public    $uploadPath    = "uploads/packages";
    protected $imageModel    = null;
    protected $imageWidth  = null;
    protected $imageHeight = null;

    public function __construct()
    {
        $this->package     = new Package();
        $this->packageEnum = new ProductEnums();
        $this->relations   = ["products"];
        $this->imageModel  = new ProductImage();
        $this->imageWidth   = config("imageUpload.product.width");
        $this->imageHeight  = config("imageUpload.product.height");
    }

    public function baseQuery()
    {
        return $this->package::query()->with($this->relations);
    }

    public function getById(Int $id)
    {
        return  $this->baseQuery()->find($id);
    }

    public function getBySlug(string $slug)
    {
        return $this->baseQuery()->where("slug", $slug)->first();
    }

    public function getAll($column = "id", $order= "desc")
    {
        return $this->baseQuery()->orderBy($column, $order)->get();
    }

    public function getAllWithPaginate(){
        return $this->baseQuery()->orderBy("id", "desc")->paginate(4);
    }

    public function getAllActive($column = "id", $order= "desc")
    {
        return $this->baseQuery()->orderBy($column, $order)->active()->get();
    }

    public function getIndexProducts(){
        return $this->baseQuery()->active()->take(12)->get();
    }

    public function getMinimumPrice(){
        return $this->baseQuery()->min("price");
    }

    public function getMaximumPrice(){
        return $this->baseQuery()->max("price");
    }
    public function store(Request $request)
    {

        $data = $request->only($this->package->getFillable());

        if($data["slug"] == null){

             $data["slug"] = Str::slug($data["title"], "-");
        }

        $data["image"] = LaraImage::upload("store", $this->uploadPath,  $data["title"],$request->file("image"), $this->imageWidth, $this->imageHeight);
        $data["user_id"] = Auth::user()->id;
        $package =  $this->baseQuery()->create($data);

        $package->products()->attach($request->product_id);

        return $package;
    }

    public function update(Request $request,$id )
    {


        $package = $this->getById($id);

        $isActive = $package->is_active;



        $data = $request->all($this->package->getFillable());

        if(!$request->hasFile("image")){
            unset($data["image"]);
        }
        if($data["slug"] == null){
            $data["slug"] = Str::slug($data["title"], "-");
        }
        if($request->hasFile("image")){

            $image = $request->file("image");
            $data["image"] = LaraImage::upload("update",$this->uploadPath,$data["title"], $image, $this->imageWidth, $this->imageHeight);

        }

        $data["is_active"] = $isActive ;
        $data["user_id"] = Auth::user()->id;


        $update = $package->update($data);

        $package->products()->sync($request->product_id);

        return $update;
    }
    public function updateStatus(Request $request)
    {
        $id = $request->id;

        $product = $this->getById($id);

        $active = $this->packageEnum::_ACTIVE_PRODUCT;

        $inActive = $this->packageEnum::_INACTIVE_PRODUCT;

        $is_active = $request->get("is_active") == $active ? $active : $inActive;

        $product->is_active = $is_active;

        return $product->save();
    }

    public function destroy(int $id)
    {
        $package = $this->getById($id);

        LaraImage::deleteUploadedFile($this->uploadPath, $package->image);

        $package->products()->detach();
        $package->delete();
    }

    public function listingMap($collect){
        return $collect->map(function ($item){
            return [
                "id" => $item->id,
                "title" => $item->title,
                "slug"  => $item->slug,
                "description" => $item->description,
                "short_description" => Str::limit($item->description, 60, "..."),
                "address" => $item->address,
                "categoryTitle" => $item->category->title,
                "price" => $item->price,
                "image" => getCoverImage($item->id),
                "ownerID" => $item->owner->id,
                "route" => route("front.product-detail", ["slug" => $item->slug])
            ];
        });
    }

    public function detailMap($item){
        return [
            "id" => $item->id,
            "title" => $item->title,
            "slug" => $item->slug,
            "description" => strip_tags($item->description),
            "price" => $item->price,
            "address" => $item->address,
            "images" => $item->images,
            "categoryID" => $item->category->id,
            "ownerPhone" => $item->owner->phone,
            "ownerName" => $item->owner->name,
            "ownerEmail" => $item->owner->email,
            "ownerID" => $item->owner->id,
            "ownerImage" => getProfileImage($item->owner->image),
            "university_id" => $item->university->id,
            "faculty_id" => $item->faculty->id
        ];
    }
}
