<?php


namespace App\Repositories\Classes;


use App\Enums\CategoryEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Menu;
use App\Repositories\Interfaces\IBaseRepository;
use Illuminate\Http\Request;


class MenuRepository implements IBaseRepository
{
    private $menu  = null;
    private $enum  = null;
    private $relations     = null;

    public function __construct()
    {
        $this->menu = new Menu();
        $this->enum = new CategoryEnums();
        $this->relations = ["children", "parent"];
    }

    public function baseQuery()
    {
        return $this->menu::query()->with($this->relations);
    }

    public function getById(Int $id)
    {
        return  $this->baseQuery()->find($id);
    }

    public function getBySlug(string $slug)
    {
        return $this->baseQuery()->where("slug", $slug)->first();
    }


    public function getAll(string $order = "id", string $by = "desc")
    {
        return $this->baseQuery()->orderBy($order, $by)->get();
    }
    public function getAllActive()
    {

        return $this->baseQuery()->active()->orderBy("title", "asc")->get();
    }

    public function getAllParents(string $order= "rank", string $by= "desc")
    {
        return $this->baseQuery()->where("parent_id", "=",null)->orWhere("parent_id", "=",0)->orderBy($order, $by)->get();
    }
    public function getAllChilds(string $order= "id", string $by= "desc")
    {
        return $this->baseQuery()->child()->orderBy($order, $by)->get();
    }
    public function store(Request $request)
    {


        $data = $request->except("categoryRadio");

        return $this->menu->create($data);
    }

    public function update(Request $request, $id)
    {


        $menu = $this->getById($id);

        $isActive = $menu->is_active == $this->enum::_ACTIVE_CATEGORY ? $this->enum::_ACTIVE_CATEGORY : $this->enum::_INACTIVE_CATEGORY;

        $data = $request->except("categoryRadio");

        if($request->hasFile("image")){

            $image = $request->file("image");
            $data["is_active"] = $isActive;

        }

        $menu->update($data);
    }
    public function updateStatus(Request $request)
    {
        $id = $request->id;

        $menu = $this->getById($id);

        $active = $this->enum::_ACTIVE_CATEGORY;

        $inActive = $this->enum::_INACTIVE_CATEGORY;

        $is_active = $request->get("is_active") == $active ? $active : $inActive;

        $menu->is_active = $is_active;

        return $menu->save();
    }

    public function destroy(int $id)
    {
        $menu = $this->getById($id);



        $menu->delete();

        RedirectHelper::RedirectWithSuccessFlashMessage("destroy","product-categories.index");
    }

    public function listingMap($collect){
        return $collect->map(function ($item){
            return [
                "id" => $item->id,
                "title" => $item->title,
                "slug"  => $item->slug,
                "route" => $item->route  == null ? "javascript:void(0);" : $item->route,
                "childCount" => $item->children()->count(),
                "children" => $this->listingMap($item->children),
            ];
        });
    }
}
