<?php


namespace App\Repositories\Classes;


use App\Enums\CategoryEnums;
use App\Enums\ProductEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Product;
use App\ProductCategory;
use App\ProductImage;
use App\Repositories\Interfaces\IBaseRepository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Murattcann\LaraImage\LaraImage;

class ProductRepository implements IBaseRepository
{
    protected $product       = null;
    protected $productEnum   = null;
    protected $relations     = null;
    public    $uploadPath    = "uploads/products";
    protected $imageModel    = null;
    protected $imageWidth  = null;
    protected $imageHeight = null;
    protected $imageRepository = null;

    public function __construct()
    {
        $this->product     = new Product();
        $this->imageRepository = new ProductImageRepository();
        $this->productEnum = new ProductEnums();
        $this->relations   = ["owner", "category", "images"];
        $this->imageModel  = new ProductImage();
        $this->imageWidth   = config("imageUpload.product.width");
        $this->imageHeight  = config("imageUpload.product.height");
    }

    public function baseQuery()
    {
        return $this->product::query()->with($this->relations);
    }

    public function getById(Int $id)
    {
        return  $this->baseQuery()->find($id);
    }

    public function getBySlug(string $slug)
    {
        return $this->baseQuery()->where("slug", $slug)->first();
    }

    public function getAll($column = "id", $order= "desc")
    {
        return $this->baseQuery()->orderBy($column, $order)->get();
    }

    public function getAllWithPaginate(){
        return $this->baseQuery()->orderBy("id", "desc")->paginate(4);
    }

    public function getAllActive($column = "id", $order= "desc")
    {
        return $this->baseQuery()->orderBy($column, $order)->active()->get();
    }

    public function getIndexProducts(){
        return $this->baseQuery()->active()->take(8)->get();
    }


    public function getFeaturedProducts(){
        return $this->baseQuery()
            ->where("is_active", "=", "1")
            ->where("is_featured", "=", "1")
            ->get();
    }

    public function getSimilars($productId){
        return $this->baseQuery()
            ->active()
            ->where("id", "!=", "$productId")
            ->take(8)
            ->get();
    }
    public function getMinimumPrice(){
        return $this->baseQuery()->min("price");
    }

    public function getMaximumPrice(){
        return $this->baseQuery()->max("price");
    }


    public function store(Request $request)
    {

        $data = $request->all();
           if($data["slug"] == null){
             $data["slug"] = Str::slug($data["title"], "-");
        }

        return  $this->product->create($data);


    }

    public function storeProductImage(Request $request)
    {

        $product = $this->getById($request->product_id);
        $title = $product->title;

        $image = $request->file("file");

        $data = [
            "title" => $title,
            "image" => LaraImage::upload("store", $this->uploadPath,$title, $image,$this->imageWidth, $this->imageHeight),
            "is_cover" => 0,
            "product_id" => $product->id
        ];

        return $images = $this->imageModel->create($data);
    }

    public function update(Request $request,$id )
    {


        $product = $this->getById($id);

        $isActive = $product->is_active == $this->productEnum::_ACTIVE_PRODUCT ? $this->productEnum::_ACTIVE_PRODUCT : $this->productEnum::_INACTIVE_PRODUCT;

        $data = $request->all();
        if($data["slug"] == null){
            $data["slug"] = Str::slug($data["title"], "-");
        }

        $product->update($data);
    }

    public function updateImageStatus(Request $request)
    {
        $id = $request->id;

        $product = $this->imageRepository->getById($id);

        $active = $this->productEnum::_ACTIVE_PRODUCT;

        $inActive = $this->productEnum::_INACTIVE_PRODUCT;

        $is_active = $request->get("is_active") == $active ? $active : $inActive;

        $product->is_active = $is_active;

        return $product->save();
    }
    public function updateStatus(Request $request)
    {
        $id = $request->id;

        $product = $this->getById($id);

        $active = $this->productEnum::_ACTIVE_PRODUCT;

        $inActive = $this->productEnum::_INACTIVE_PRODUCT;

        $is_active = $request->get("is_active") == $active ? $active : $inActive;

        $product->is_active = $is_active;

        return $product->save();
    }

    public function setForward(Request $request){

        $id = $request->id;

        $product = $this->getById($id);

        return $product->update(["is_featured" => $request->is_featured]);
    }

    public function destroy(int $id)
    {
        $product = $this->getById($id);
        foreach ($product->images as $image)
        {
            LaraImage::deleteUploadedFile($this->uploadPath, $image);
        }

        $product->delete();
    }

    public function listingMap($collect){
        return $collect->map(function ($item){

            return [
                "id" => $item->id,
                "title" => $item->title,
                "slug"  => $item->slug,
                "description" => $item->description,
                "shortDescription" => Str::limit($item->description, 60, "..."),
                "categoryTitle" => $item->category->title,
                "price" => $item->price,
                "discountPrice" => $item->discount_price,
                "coverImage" => getImage("products", $this->imageRepository->getCoverImage( $item->id)->image),
                "secondCover" => getImage("products", $this->imageRepository->getSecondCoverImage($item->id)->image),
                "route" => route("front.product-detail", ["slug" => $item->slug])
            ];
        });
    }

    public function detailMap($item){

        $exp = explode(".", $item->description);

        $shortDescription = $exp[0]. '.' .@$exp[1]. '.' .@$exp[2];

        return [
            "id" => $item->id,
            "title" => $item->title,
            "slug" => $item->slug,
            "description" => $item->description,
            "shortDescription" => $shortDescription,
            "price" => $item->price,
            "discountPrice" => $item->discount_price,
            "address" => $item->address,
            "images" => $item->images,
            "categoryID" => $item->category->id,
            "categoryTitle" => $item->category->title,
            "categoryRoute" => "#"
        ];
    }
}
