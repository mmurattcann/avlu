<?php


namespace App\Repositories\Classes;


use App\Enums\CategoryEnums;
use App\FooterItem;
use App\Helpers\RedirectHelper\RedirectHelper;

use App\Repositories\Interfaces\IBaseRepository;
use Illuminate\Http\Request;


class FooterItemRepository
{
    private$item  = null;
    private $footerItem  = null;
    private $enum  = null;
    private $itemRelations = null;

    public function __construct()
    {

        $this->footerItem = new FooterItem();
        $this->enum = new CategoryEnums();
        $this->itemRelations = ["parent"];
    }


    public function itemBaseQuery()
    {
        return $this->footerItem::query()->with($this->itemRelations);
    }

    public function getItemById(Int $id)
    {
        return  $this->itemBaseQuery()->find($id);
    }

    public function getItemBySlug(string $slug)
    {
        return $this->itemBaseQuery()->where("slug", $slug)->first();
    }


    public function getAllItem(string $order = "id", string $by = "desc")
    {
        return $this->itemBaseQuery()->orderBy($order, $by)->get();
    }

    public function  getAllByParent($parentId){
        return $this->itemBaseQuery()->where("parent_id", $parentId)->get();
    }

    public function getAllActiveItem()
    {

        return $this->itemBaseQuery()->active()->orderBy("title", "asc")->get();
    }

    public function getAllChilds(string $order= "id", string $by= "desc")
    {
        return $this->itemBaseQuery()->children()->orderBy($order, $by)->get();
    }
    public function store(Request $request)
    {


        $data = $request->only($this->footerItem->getFillable());

        return $this->footerItem->create($data);
    }

    public function update(Request $request, $id)
    {


       $item = $this->getItemById($id);

        $isActive =$item->is_active == $this->enum::_ACTIVE_CATEGORY ? $this->enum::_ACTIVE_CATEGORY : $this->enum::_INACTIVE_CATEGORY;

        $data = $request->except("categoryRadio");

        if($request->hasFile("image")){

            $image = $request->file("image");
            $data["is_active"] = $isActive;

        }

       return $item->update($data);
    }
    public function updateStatus(Request $request)
    {
        $id = $request->id;

       $item = $this->getItemById($id);

        $active = $this->enum::_ACTIVE_CATEGORY;

        $inActive = $this->enum::_INACTIVE_CATEGORY;

        $is_active = $request->get("is_active") == $active ? $active : $inActive;

       $item->is_active = $is_active;

        return$item->save();
    }

    public function destroyItem(int $id)
    {
       $item = $this->getItemById($id);



       $item->delete();

        RedirectHelper::RedirectWithSuccessFlashMessage("destroy","menus.index");
    }

    public function listingMap($collect){
        return $collect->map(function ($item){
            return [
                "id" => $item->id,
                "title" => $item->title,
                "slug"  => $item->slug,
                "route" => $item->route  == null ? "javascript:void(0);" : $item->route,
            ];
        });
    }
}
