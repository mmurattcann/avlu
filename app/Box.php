<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    protected $fillable = ["title", "image","description", "route", "rank", "is_active", "button_text"];
}
