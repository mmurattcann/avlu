<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class BlogCategory extends Model
{
    protected $guarded = ["id"];

    public function blogs(){

        return $this->hasMany(Blog::class);
    }

    public function getShortenDescriptionAttribute(){
        return Str::limit($this->description, 30, "...");
    }
}
