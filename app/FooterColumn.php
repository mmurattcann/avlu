<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FooterColumn extends Model
{
    protected $fillable = ["title", "is_active", "rank"];

    public function children(){

       return  $this->hasMany(FooterItem::class, "parent_id");
    }
}
