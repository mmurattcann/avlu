<div class="app-header header py-1 d-flex">
    <div class="container-fluid">
        <div class="d-flex">
            <a class="header-brand" href="{{route("dashboard")}}">
                <img src="{{$siteLogo}}" class="header-brand-img text-center ml-5 d-none d-sm-block" alt="Spain logo"><img src="{{panelAsset("images/brand/logo.png")}}" class="header-brand-img-2 d-sm-none" alt="Spain logo">
            </a>
            <a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#"></a>

            <div class="d-flex order-lg-2 ml-auto">
                <form class="form-inline mr-auto">
                    <div class="search-element">
                        <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                <div class="dropdown d-none d-md-flex " >
                    <a  class="nav-link icon full-screen-link">
                        <i class="mdi mdi-arrow-expand-all"  id="fullscreen-button"></i>
                    </a>
                </div>

                <div class="dropdown">
                    <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                        <span class="avatar avatar-md brround"><img src="{{$siteLogo}}" alt="Profile-img" class="avatar avatar-md brround"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow ">
                        <div class="text-center">
                            <a href="#" class="dropdown-item text-center font-weight-sembold user">{{Auth::user()->name}}</a>

                            <div class="dropdown-divider"></div>
                        </div>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="e.preventDefault(); document.getElementById('logout-form').submit();" title="Çıkış Yap" >
                            <i class="dropdown-icon mdi  mdi-logout-variant"></i> Çıkış Yap
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                            @method("POST")
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
