<aside class="app-sidebar">
    <div class="app-sidebar__user">
        <div class="dropdown user-pro-body">
            <div>
                <img src="{{getProfileImage(Auth::user()->image)}}" alt="user-img" class="avatar avatar-xl brround mCS_img_loaded" width="100%">
                <a href="editprofile.html" class="profile-img">
                    <span class="fa fa-pencil" aria-hidden="true"></span>
                </a>
            </div>
            <div class="user-info mb-2">
                <h4 class="font-weight-semibold text-dark mb-1">{{\Illuminate\Support\Facades\Auth::user()->name}}</h4>
                <span class="mb-0 text-muted">Admin</span>
            </div>
            <a href="{{route("options.edit")}}" title="settings" class="user-button"><i class="fa fa-cog"></i></a>
            <a href="{{ route('logout') }}" onclick="e.preventDefault(); document.getElementById('logout-form').submit();" title="Çıkış Yap" class="user-button"><i class="fa fa-power-off"></i></a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
                @method("POST")
            </form>
        </div>
    </div>

    <ul class="side-menu">
        <li>
            <a class="side-menu__item" href="{{route("dashboard")}}"><i class="side-menu__icon fa fa-desktop"></i><span class="side-menu__label">Anasayfa</span></a>
        </li>

        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon mdi mdi-account-multiple"></i><span class="side-menu__label">Kullanıcı Yönetimi</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li>
                    <a href="{{route("users.index")}}" class="slide-item">Kullanıcı Listesi</a>
                </li>
                <li>
                    <a href="{{route("users.create")}}" class="slide-item">Kullanıcı Ekle</a>
                </li>
            </ul>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-bars"></i><span class="side-menu__label">Ürün Kategori Yönetimi</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li>
                    <a href="{{route("product-categories.index")}}" class="slide-item">Kategori Listesi</a>
                </li>
                <li>
                    <a href="{{route("product-categories.create")}}" class="slide-item">Kategori Ekle</a>
                </li>
            </ul>
        </li>

        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-cubes"></i><span class="side-menu__label">Ürün Yönetimi</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li>
                    <a href="{{route("products.index")}}" class="slide-item">Ürün Listesi</a>
                </li>
                <li>
                    <a href="{{route("products.create")}}" class="slide-item">Ürün Ekle</a>
                </li>
                <li>
                    <a href="{{route("packages.index")}}" class="slide-item">Paket Listesi</a>
                </li>
                <li>
                    <a href="{{route("packages.create")}}" class="slide-item">Paket Ekle</a>
                </li>
            </ul>
        </li>

        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-sticky-note"></i><span class="side-menu__label">Sayfa Yönetimi</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li>
                    <a href="{{route("pages.index")}}" class="slide-item">Sayfa Listesi</a>
                </li>
                <li>
                    <a href="{{route("pages.create")}}" class="slide-item">Sayfa Ekle</a>
                </li>
            </ul>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-bullseye"></i><span class="side-menu__label">Blog Kategori Yönetimi</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li>
                    <a href="{{route("blog-categories.index")}}" class="slide-item">Kategori Listesi</a>
                </li>
                <li>
                    <a href="{{route("blog-categories.create")}}" class="slide-item">Kategori Ekle</a>
                </li>
            </ul>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-bullseye"></i><span class="side-menu__label">Blog Yönetimi</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li>
                    <a href="{{route("blogs.index")}}" class="slide-item">Blog Listesi</a>
                </li>
                <li>
                    <a href="{{route("blogs.create")}}" class="slide-item">Blog Ekle</a>
                </li>
            </ul>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-image"></i><span class="side-menu__label">Slider Yönetimi</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li>
                    <a href="{{route("sliders.index")}}" class="slide-item">Slider Listesi</a>
                </li>
                <li>
                    <a href="{{route("sliders.create")}}" class="slide-item">Slider Ekle</a>
                </li>
            </ul>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-image"></i><span class="side-menu__label">Kutu Yönetimi</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li>
                    <a href="{{route("boxes.index")}}" class="slide-item">Kutu Listesi</a>
                </li>
                <li>
                    <a href="{{route("boxes.create")}}" class="slide-item">Kutu Ekle</a>
                </li>
            </ul>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-image"></i><span class="side-menu__label">Menü Yönetimi</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li>
                    <a href="{{route("menus.index")}}" class="slide-item">Menü Listesi</a>
                </li>
                <li>
                    <a href="{{route("menus.create")}}" class="slide-item">Menü Ekle</a>
                </li>
                <li>
                    <a href="{{route("footer-columns.index")}}" class="slide-item">Footor Kolonları</a>
                </li>
            </ul>
        </li>
    </ul>
</aside>

