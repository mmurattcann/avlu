@extends("layouts.panel._layout")
@section("title")
    {{$title}}
@endsection

@push("css")
    <!-- DROPZone -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
    <!-- select2 Plugin -->
    <link href="{{panelAsset("plugins/select2/select2.min.css")}}" rel="stylesheet" />

    <!-- WYSIWYG Editor css -->
    <link href="{{panelAsset("plugins/wysiwyag/richtext.css")}}" rel="stylesheet" />
    <!-- forn-wizard css-->
    <link href="{{panelAsset("plugins/forn-wizard/css/material-bootstrap-wizard.css")}}" rel="stylesheet" />
    <link href="{{panelAsset("plugins/forn-wizard/css/demo.css")}}" rel="stylesheet" />

   <style>
       .wizard-ul {
           list-style: none;
           text-align: center;
       }

       .wizard-ul li {
           display: inline-block;
       }
   </style>
@endpush

@section("content")
    <ol class="breadcrumb breadcrumb-arrow mt-3 mb-3">
    </ol>
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    <div class="card-title"> {{$title}}</div>
                </div>
                <div class="card-body p-6">
                    <div class="row">
                        <form action="{{route("products.storeProduct")}}" id="product-form" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method("POST")
                            <input type="hidden" name="is_active" value="1">
                            <div class="row">

                                    <div class="col-md-12 mt-2 mb-2">
                                        <ul class="wizard-ul">
                                            <li>
                                                <a href="javascript:void(0);" class="btn btn-primary">Ürün Bilgileri</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);" class="btn btn-light">Ürün Görselleri</a>
                                            </li>
                                        </ul>
                                    </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Kategori</label>
                                        <select class="form-control select2 custom-select" id="category_id" name="category_id">

                                            <option value="">--- Seçim Yapın ---</option>
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->title}}</option>
                                            @endforeach
                                        </select>
                                        @error("category_id")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Başlık</label>
                                        <input type="text" class="form-control" name="title" placeholder="ÖRN: Satılık Telefon" value="{{old("title")}}">
                                        @error("title")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label class="form-label">Slug (SEO URL için)</label>
                                            <input type="text" class="form-control" name="slug" placeholder="ÖRN: satilik-telefon" value="{{old("slug")}}">
                                            @error("slug")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label class="form-label">Fiyat</label>
                                            <input type="text" class="form-control" name="price" placeholder="ÖRN: 250" value="{{old("price")}}">
                                            @error("price")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label class="form-label">İndirimli Fiyat</label>
                                            <input type="text" class="form-control" name="discount_price" placeholder="ÖRN: 250" value="{{old("price")}}">
                                            @error("discount_price")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Açıklama</label>
                                    <textarea name="description" class="content">{{old("description")}}</textarea>
                                    @error("description")
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>

                            </div>

                            <div class="col-md-12 text-center">
                                <button class="btn btn-primary" type="submit"> Sonraki Adım</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{route("products.store")}}"
                                  method="POST"
                                  id="product-form"
                                  enctype="multipart/form-data">
                                @csrf
                                @method("POST")

                                <input type="hidden" name="is_active" value="1">
                                <input type="hidden" name="user_id" value="{{Auth::id()}}">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">Kategori</label>
                                            <select class="form-control select2 custom-select" id="category_id" name="category_id">

                                                <option value="">--- Seçim Yapın ---</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->title}}</option>
                                                @endforeach
                                            </select>
                                            @error("category_id")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">Başlık</label>
                                            <input type="text" class="form-control" name="title" placeholder="ÖRN: Satılık Telefon" value="{{old("title")}}">
                                            @error("title")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label class="form-label">Slug (SEO URL için)</label>
                                                <input type="text" class="form-control" name="slug" placeholder="ÖRN: satilik-telefon" value="{{old("slug")}}">
                                                @error("slug")
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label class="form-label">Fiyat</label>
                                                <input type="text" class="form-control" name="price" placeholder="ÖRN: 250" value="{{old("price")}}">
                                                @error("price")
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Açıklama</label>
                                    <textarea name="description" class="form-control">{{old("description")}}</textarea>
                                    @error("description")
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Adres</label>
                                    <textarea name="address" class="form-control">{{old("address")}}</textarea>
                                    @error("address")
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>

                                <button class="btn btn-primary btn-lg mt-5 mb-5" type="submit">Kaydet</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>--}}
@endsection

@push("js")
    <!-- forn-wizard js-->
    <script src="{{panelAsset("plugins/forn-wizard/js/material-bootstrap-wizard.js")}}"></script>
    <script src="{{panelAsset("plugins/forn-wizard/js/jquery.validate.min.js")}}"></script>
    <script src="{{panelAsset("plugins/forn-wizard/js/jquery.bootstrap.js")}}"></script>
    <script src="{{panelAsset("plugins/bootstrap-wizard/jquery.bootstrap.wizard.js")}}"></script>
    <script src="{{panelAsset("plugins/bootstrap-wizard/wizard.js")}}"></script>
    <!--Select2 js -->
    <script src="{{panelAsset("plugins/select2/select2.full.min.js")}}"></script>
    <script src="{{panelAsset("js/select2.js")}}"></script>
    <script src="{{panelAsset("js/dropzone.js")}}"></script>
    <script src="{{panelAsset("CustomOperations/productDZUpload.js")}}"></script>
    <!-- CKE EDItor -->
    <script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>

    <!-- WYSIWYG Editor js -->
    <script src="{{panelAsset("plugins/wysiwyag/jquery.richtext.js")}}"></script>
    <script src="{{panelAsset("plugins/wysiwyag/richText1.js")}}"></script>
@endpush
