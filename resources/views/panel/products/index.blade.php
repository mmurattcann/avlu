@extends("layouts.panel._layout")
@section("title")
{{$title}}
@endsection
@push("css")
    <!-- Data table css -->
    <link href="{{panelAsset("plugins/datatable/dataTables.bootstrap4.min.css")}}" rel="stylesheet" />
    <!-- Bootstrap Toggle css -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <!---Sweetalert Css-->
    <link href="{{panelAsset("plugins/sweet-alert/jquery.sweet-modal.min.css")}}" rel="stylesheet" />
    <link href="{{panelAsset("plugins/sweet-alert/sweetalert.css")}}" rel="stylesheet" />
@endpush

@section("content")
    <div class="container">
        <ol class="breadcrumb breadcrumb-arrow mt-3">
        </ol>
        <div class="page-header">
            <h4 class="page-title">{{$title}}</h4>
            <a href="{{route("products.create")}}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-user-plus"></i> Yeni Kayıt</a>

        </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Kategoriler </div>
                    </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="users-table" class="table table-striped table-bordered w-100">
                            <thead>
                                <tr>
                                    <th class="wd-15p">#ID</th>
                                    <th class="wd-15p">Kapak</th>
                                    <th class="wd-10p">Başlık</th>
                                    <th class="wd-10p">Açıklama</th>
                                    <th class="wd-20p">Kategori</th>
                                    <th class="wd-20p">Görseller</th>
                                    <th class="wd-10p">Aktif Mi?</th>
                                    <th class="wd-10p">Öne Çıkar</th>
                                    <th class="wd-50p">İşlemler</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr id="tr-{{$product->id}}">
                                    <td>{{$product->id}}</td>
                                    <td>
                                        <img src="{{getCoverImage($product->id)}}" class="img-responsive" style="width: 100px; height: 80px;">
                                    </td>

                                    <td>{{$product->title}}</td>
                                    <td>{!! getShortString($product->description,30) !!}...</td>
                                    <td>
                                          <span class="bg-pink p-1 txt-white rounded">
                                           {{$product->category->title}}
                                        </span>
                                    </td>
                                    <td>
                                        <a href="{{ route("products.images", $product->id)  }}" class="btn btn-outline-primary">
                                            Ürün Görselleri
                                        (<small> {{count($product->images)}} </small>)
                                        </a>
                                    </td>
                                    <td>
                                        <input type="checkbox"
                                               name="is_active"
                                               id="is_active"
                                               class="is_active"
                                               data-id="{{$product->id}}"
                                               data-toggle="toggle"
                                               data-on="Aktif"
                                               data-off="Pasif"
                                               data-onstyle="success"
                                               data-offstyle="danger"
                                               data-route="{{route("updateProductStatus")}}"
                                               @if($product->is_active == $isActive) checked @endif
                                        >
                                    </td>
                                    <td>
                                        <input type="checkbox"
                                               name="is_featured"
                                               id="is_featured"
                                               class="is_featured"
                                               data-id="{{$product->id}}"
                                               data-toggle="toggle"
                                               data-on="Evet"
                                               data-off="Hayır"
                                               data-onstyle="success"
                                               data-offstyle="danger"
                                               data-route="{{route("forwardProduct")}}"
                                               @if($product->is_featured == $isActive) checked @endif
                                        >
                                    </td>
                                    <td>
                                        <a href="{{route('products.edit', $product->id)}}" class="btn btn-outline-success" ><i class="fa fa-pencil-square-o fa-3x"></i></a>
                                        <button class="btn btn-danger delete-button" data-id="{{$product->id}}" data-route="{{route("products.destroy", $product->id)}}" >Sil</button>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
            <!-- section-wrapper -->

        </div>
    </div>
    </div>
@endsection

@push("js")
    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <!-- Data tables -->
    <script src="{{panelAsset("plugins/datatable/jquery.dataTables.min.js")}}"></script>
    <script src="{{panelAsset("plugins/datatable/dataTables.bootstrap4.min.js")}}"></script>
    <script src="{{panelAsset("plugins/datatable/datatable.js")}}"></script>

    <!-- Sweet alert Plugin -->
    <script src="{{panelAsset("plugins/sweet-alert/jquery.sweet-modal.min.js")}}"></script>
    <script src="{{panelAsset("plugins/sweet-alert/sweetalert.min.js")}}"></script>
    <script src="{{panelAsset("js/sweet-alert.js")}}"></script>

    <!-- User CRUD Operations-->
    <script src="{{panelAsset("CustomOperations/productCategoryCrud.js")}}"></script>
    <script src="{{panelAsset("CustomOperations/forward-product.js")}}"></script>

@endpush
