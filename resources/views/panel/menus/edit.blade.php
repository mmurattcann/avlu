@extends("layouts.panel._layout")
@section("title")
    {{$title}}
@endsection

@push("css")

    <!-- select2 Plugin -->
    <link href="{{panelAsset("plugins/select2/select2.min.css")}}" rel="stylesheet" />

@endpush

@section("content")
    <ol class="breadcrumb breadcrumb-arrow mt-3 mb-3">
    </ol>

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{route("menus.update", $menu->id)}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method("PUT")
                                <input type="hidden" name="is_active" value="1">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="form-label">Tipi</label>
                                        <label class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" name="categoryRadio" value="1" checked>
                                            <span class="custom-control-label">Ana Kategori</span>
                                        </label>
                                        <label class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" name="categoryRadio" value="2">
                                            <span class="custom-control-label">Alt Kategori</span>
                                        </label>
                                    </div>
                                    <div class="form-group col-md-12 select-category-area">
                                        <label class="form-label">Kategoriler</label>
                                        <select class="form-control select2 custom-select" id="parent_id" name="parent_id">

                                            <option value="">--- Seçim Yapın ---</option>
                                            @foreach($parents as $parent)
                                                <option value="{{$parent->id}}" {{ $parent->id == $menu->parent_id ?? "selected" }}>{{$parent->title}}</option>
                                            @endforeach
                                        </select>
                                        @error("parent_id")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="form-label">Sıra</label>
                                        <input type="text" class="form-control" name="rank" placeholder="ÖRN: 1" value="{{old("rank", $menu->rank)}}">
                                        @error("rank")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="form-label">Başlık</label>
                                        <input type="text" class="form-control" name="title" placeholder="ÖRN: Ürünlerimiz" value="{{old("title", $menu->title)}}">
                                        @error("title")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="form-label">Slug (Seo URL için)</label>
                                        <input type="text" class="form-control" name="slug" placeholder="ÖRN: urunlerimiz" value="{{old("slug", $menu->slug)}}">
                                        @error("slug")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="form-label">Rota</label>
                                        <input type="text" class="form-control" name="route" placeholder="ÖRN: akilli-telefon" value="{{old("route", $menu->route)}}">
                                        @error("route")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="form-label">Açıklama</label>
                                        <textarea name="description" class="form-control">{{old("description", $menu->description)}}</textarea>
                                        @error("description")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>


                                    <button class="btn btn-primary btn-lg mt-5 mb-5" type="submit">Kaydet</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("js")

    <!--Select2 js -->
    <script src="{{panelAsset("plugins/select2/select2.full.min.js")}}"></script>
    <script src="{{panelAsset("js/select2.js")}}"></script>
    <script>
        $(function () {

            $('.select-category-area').hide();

            $('input:radio[name="categoryRadio"]').change(
                function(){
                    if ($(this).is(':checked') && $(this).val() == 2) {

                        $('.select-category-area').show();
                    }else  if ($(this).is(':checked') && $(this).val() == 1) {

                        $('.select-category-area').hide();
                        $('#parent_id').prop('selectedIndex',0);
                    }
                });
        })
    </script>
@endpush
