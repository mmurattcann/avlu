@extends("layouts.panel._layout")
@section("title")
    {{$title}}
@endsection

@push("css")
    <!-- WYSIWYG Editor css -->
    <link href="{{panelAsset('plugins/wysiwyag/richtext.css')}}" rel="stylesheet">
@endpush

@section("content")
    <ol class="breadcrumb breadcrumb-arrow mt-3 mb-3">

    </ol>

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{route("boxes.update", $box->id)}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method("PUT")
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">Mevcut Kapak Resmi</label>
                                            <img src="{{getImage("boxes", $box->image)}}" class="original-image" alt="{{$box->title}}" style="width:100px;height: 100px; ">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" name="profilePictureRadio" value="1" checked>
                                        <span class="custom-control-label">Orijinali Koru</span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" name="profilePictureRadio" value="2">
                                        <span class="custom-control-label">Değiştir</span>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group file-input-area">
                                        <div class="form-label">Kapak Resmini Seç</div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="image">
                                            <label class="custom-file-label">Seç</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-5">

                                    <div class="form-group col-md-4">
                                        <label class="form-label">Sıra</label>
                                        <input type="number" class="form-control" name="rank" placeholder="Sıra" value="{{old("rank", $box->rank)}}">
                                        @error("rank")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="form-label">Başlık</label>
                                        <input type="text" class="form-control" name="title" placeholder="Başlık" value="{{old("title", $box->title)}}">
                                        @error("title")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="form-label"> Buton Yazısı</label>
                                        <input type="text" class="form-control" name="button_text" placeholder="Örn: Hemen İncele" value="{{old("button_text", $box->button_text)}}">
                                        @error("button_text")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="form-label"> Rota</label>
                                        <input type="text" class="form-control" name="route" placeholder="Örn: https://wwww.avluhomegarden.com/..." value="{{old("route", $box->route)}}">
                                        @error("route")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">

                                </div>

                                <div class="form-group">
                                    <label class="form-label">İçerik ( <smal> Çok Uzun Olmamalı</smal>)</label>
                                    <textarea class="form-control content richText-initial" name="description" placeholder="İçerik">
                                        {{old("content", $box->description)}}
                                    </textarea>
                                    @error("description")
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                {{-- <div class="form-group ">
                                     <label class="form-label">Kullanıcı Rolü</label>
                                     <select class="form-control select2 custom-select" name="role">

                                         <option value="{{$standardUserEnum}}">Standart Kullanıcı</option>
                                         <option value="{{$adminEnum}}">Admin</option>
                                     </select>
                                     @error("role")
                                         <span class="text-danger">{{$message}}</span>
                                     @enderror
                                 </div>--}}
                                <button class="btn btn-primary btn-lg mt-5 mb-5" type="submit">Kaydet</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("js")
    <!-- WYSIWYG Editor js -->
    <script src="{{panelAsset('plugins/wysiwyag/jquery.richtext.js')}}"></script>
    <script src="{{panelAsset('plugins/wysiwyag/richText1.js')}}"></script>
    <script src="{{panelAsset('CustomOperations/editImageChanger.js')}}"></script>
@endpush
