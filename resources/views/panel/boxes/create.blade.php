@extends("layouts.panel._layout")
@section("title")
    {{$title}}
@endsection

@push("css")

    <!-- select2 Plugin -->
    <link href="{{panelAsset("plugins/select2/select2.min.css")}}" rel="stylesheet" />
    <!-- WYSIWYG Editor css -->
    <link href="{{panelAsset('plugins/wysiwyag/richtext.css')}}" rel="stylesheet">
@endpush

@section("content")
    <ol class="breadcrumb breadcrumb-arrow mt-3 mb-3">
    </ol>

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{route("boxes.store")}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method("POST")
                                <input type="hidden" name="is_active" value="1">

                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <div class="form-label">Kapak Resmi</div>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="image">
                                                <label class="custom-file-label">Seç</label>
                                            </div>

                                            @error("image")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label class="form-label">Sıra</label>
                                        <input type="number" class="form-control" name="rank" placeholder="Sıra" value="{{old("rank")}}">
                                        @error("rank")
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="form-label">Başlık</label>
                                        <input type="text" class="form-control" name="title" placeholder="Başlık" value="{{old("title")}}">
                                        @error("title")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="form-label"> Buton Yazısı</label>
                                        <input type="text" class="form-control" name="button_text" placeholder="Örn: Hemen İncele" value="{{old("button_text")}}">
                                        @error("button_text")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="form-label"> Rota</label>
                                        <input type="text" class="form-control" name="route" placeholder="Örn: https://wwww.avluhomegarden.com/..." value="{{old("route")}}">
                                        @error("route")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label">Açıklama ( <small class="text-info"> Çok uzun olmamalı </small>) </label>
                                    <textarea class="form-control content richText-initial" name="description" placeholder="Açıklama">
                                        {{old("description")}}
                                    </textarea>
                                    @error("description")
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                               {{-- <div class="form-group ">
                                    <label class="form-label">Kullanıcı Rolü</label>
                                    <select class="form-control select2 custom-select" name="role">

                                        <option value="{{$standardUserEnum}}">Standart Kullanıcı</option>
                                        <option value="{{$adminEnum}}">Admin</option>
                                    </select>
                                    @error("role")
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>--}}
                                <button class="btn btn-primary btn-lg mt-5 mb-5" type="submit">Kaydet</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("js")

    <!--Select2 js -->
    <script src="{{panelAsset("plugins/select2/select2.full.min.js")}}"></script>
    <script src="{{panelAsset("js/select2.js")}}"></script>
    <!-- WYSIWYG Editor js -->
    <script src="{{panelAsset('plugins/wysiwyag/jquery.richtext.js')}}"></script>
    <script src="{{panelAsset('plugins/wysiwyag/richText1.js')}}"></script>
@endpush
