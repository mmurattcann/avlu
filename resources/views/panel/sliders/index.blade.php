@extends("layouts.panel._layout")
@section("title")
{{$title}}
@endsection
@push("css")
    <!-- Data table css -->
    <link href="{{panelAsset("plugins/datatable/dataTables.bootstrap4.min.css")}}" rel="stylesheet" />
    <!-- Bootstrap Toggle css -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <!---Sweetalert Css-->
    <link href="{{panelAsset("plugins/sweet-alert/jquery.sweet-modal.min.css")}}" rel="stylesheet" />
    <link href="{{panelAsset("plugins/sweet-alert/sweetalert.css")}}" rel="stylesheet" />
@endpush

@section("content")
    <div class="container">
        <ol class="breadcrumb breadcrumb-arrow mt-3">
        </ol>
        <div class="page-header">
            <h4 class="page-title">{{$title}}</h4>
            <a href="{{route("sliders.create")}}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-user-plus"></i> Yeni Kayıt</a>

        </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">{{$title}} </div>
                    </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="users-table" class="table table-striped table-bordered w-100 datatable">
                            <thead>
                                <tr>
                                    <th class="wd-15p">#ID</th>
                                    <th class="wd-15p">Görsel</th>
                                    <th class="wd-15p">Başlık</th>
                                    <th class="wd-10p">Sıra</th>
                                    <th class="wd-10p">Buton Durumu</th>
                                    <th class="wd-10p">Durum</th>
                                    <th class="wd-50p">İşlemler</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($sliders as $slider)
                                <tr id="tr-{{$slider->id}}">
                                    <td>{{$slider->id}}</td>
                                    <td>
                                        <img src="{{getSliderImage($slider->image)}}" alt="{{$slider->title}}" class="img-responsive" style="width: 125px; height: 100px">
                                    </td>
                                    <td>{{$slider->title}}</td>
                                    <td>{{$slider->rank}}</td>
                                    <td>{{$slider->button_status}}</td>
                                    <td>
                                        <input type="checkbox"
                                               name="is_active"
                                               id="is_active"
                                               class="is_active"
                                               data-id="{{$slider->id}}"
                                               data-toggle="toggle"
                                               data-on="Aktif"
                                               data-off="Pasif"
                                               data-onstyle="success"
                                               data-offstyle="danger"
                                               data-route="{{route("updateSliderStatus")}}"
                                               @if($slider->is_active == $isActive) checked @endif
                                        >
                                    </td>
                                    <td>
                                        <a href="{{route('sliders.edit', $slider->id)}}" class="btn btn-outline-success" ><i class="fa fa-pencil-square-o fa-3x"></i></a>
                                        <button class="btn btn-danger delete-button" data-id="{{$slider->id}}" data-route="{{route("sliders.destroy", $slider->id)}}" >Sil</button>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
            <!-- section-wrapper -->

        </div>
    </div>
    </div>
@endsection

@push("js")
    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <!-- Data tables -->
    <script src="{{panelAsset("plugins/datatable/jquery.dataTables.min.js")}}"></script>
    <script src="{{panelAsset("plugins/datatable/dataTables.bootstrap4.min.js")}}"></script>
    <script src="{{panelAsset("plugins/datatable/datatable.js")}}"></script>
    <!-- Sweet alert Plugin -->
    <script src="{{panelAsset("plugins/sweet-alert/jquery.sweet-modal.min.js")}}"></script>
    <script src="{{panelAsset("plugins/sweet-alert/sweetalert.min.js")}}"></script>
    <script src="{{panelAsset("js/sweet-alert.js")}}"></script>

    <!-- User CRUD Operations-->
    <script src="{{panelAsset("CustomOperations/userCrud.js")}}"></script>

@endpush
