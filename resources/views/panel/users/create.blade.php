@extends("layouts.panel._layout")
@section("title")
    {{$title}}
@endsection

@push("css")

    <!-- select2 Plugin -->
    <link href="{{panelAsset("plugins/select2/select2.min.css")}}" rel="stylesheet" />

@endpush

@section("content")
    <ol class="breadcrumb breadcrumb-arrow mt-3 mb-3">
    </ol>

    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{route("users.store")}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method("POST")
                                <input type="hidden" name="is_active" value="1">
                                <div class="form-group">
                                    <div class="form-label">Profil Resmi</div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="image">
                                        <label class="custom-file-label">Seç</label>
                                    </div>

                                    @error("image")
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label class="form-label">Ad Soyad</label>
                                    <input type="text" class="form-control" name="name" placeholder="Ad Soyad" value="{{old("name")}}">
                                    @error("name")
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-label">E-Posta</label>
                                    <input type="email" class="form-control" name="email" placeholder="E-Posta Adresi" value="{{old("email")}}">
                                    @error("email")
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Telefon Numarası</label>
                                    <input type="text" class="form-control" name="phone" placeholder="Telefon Numarası" value="{{old("phone")}}">
                                    @error("phone")
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                    <div class="form-group">
                                        <label class="form-label">Hakkında</label>
                                        <textarea name="about" class="form-control">{{old("about")}}</textarea>
                                        @error("about")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                <div class="form-group">
                                    <label class="form-label">Şifre</label>
                                    <input type="password" class="form-control" name="password" placeholder="Şifre">
                                    @error("password")
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Şifre Tekrar</label>
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="Şifre Tekrar">
                                    @error("password_confirmation")
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Rol</label>
                                    <select name="role" class="form-control">
                                        <option value="0">Standart Kullanıcı</option>
                                        <option value="1">Admin</option>
                                    </select>
                                </div>
                               {{-- <div class="form-group ">
                                    <label class="form-label">Kullanıcı Rolü</label>
                                    <select class="form-control select2 custom-select" name="role">

                                        <option value="{{$standardUserEnum}}">Standart Kullanıcı</option>
                                        <option value="{{$adminEnum}}">Admin</option>
                                    </select>
                                    @error("role")
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>--}}
                                <button class="btn btn-primary btn-lg mt-5 mb-5" type="submit">Kaydet</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("js")

    <!--Select2 js -->
    <script src="{{panelAsset("plugins/select2/select2.full.min.js")}}"></script>
    <script src="{{panelAsset("js/select2.js")}}"></script>
@endpush
