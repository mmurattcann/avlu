@extends("layouts.panel._layout")
@section("title")
    {{$title}}
@endsection

@push("css")
    <!-- WYSIWYG Editor css -->
    <link href="{{panelAsset('plugins/wysiwyag/richtext.css')}}" rel="stylesheet">
@endpush

@section("content")
    <ol class="breadcrumb breadcrumb-arrow mt-3 mb-3">

    </ol>

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{route("options.update", $option->id)}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method("PUT")
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">Mevcut Logo</label>
                                            <img src="{{getLogo($option->logo)}}" class="original-image" alt="{{$option->site_title}}" style="width:100px;height: 100px; ">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" name="profilePictureRadio" value="1" checked>
                                        <span class="custom-control-label">Orijinali Koru</span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" name="profilePictureRadio" value="2">
                                        <span class="custom-control-label">Değiştir</span>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group file-input-area">
                                        <div class="form-label">Logo Seç</div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="logo">
                                            <label class="custom-file-label">Seç</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="title" class="form-label font-weight-bold">Site Başlığı</label>
                                            <input  value="{{old('site_title', $option->site_title)}}" class="form-control" type="text" name="site_title" id="title">
                                            @error("site_title")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="title" class="form-label font-weight-bold">İletişim E-Postası</label>
                                            <input  value="{{old('contact_email', $option->contact_email)}}" class="form-control" type="email" name="contact_email" id="title">
                                            @error("contact_email")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label for="title" class="form-label font-weight-bold">Bilgi E-Postası</label>
                                            <input  value="{{old('info_email', $option->info_email)}}" class="form-control" type="email" name="info_email" id="title">
                                            @error("info_email")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="title" class="form-label font-weight-bold">Cep Telefonu Numarası</label>
                                            <input  value="{{old('gsm', $option->gsm)}}" class="form-control" type="text" name="gsm" id="title">
                                            @error("gsm")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="title" class="form-label font-weight-bold">Sabit Telefon Numarası</label>
                                            <input  value="{{old('phone', $option->phone)}}" class="form-control" type="text" name="phone" id="title">
                                            @error("phone")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="description" class="form-label font-weight-bold">Adres</label>
                                            <textarea name="address" id="address" class="form-control">{{old('address', $option->address)}}</textarea>
                                            @error("address")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="description" class="form-label font-weight-bold">Google Map</label>
                                            <textarea name="google_map" id="google_map" class="form-control">{{old('google_map', $option->google_map)}}</textarea>
                                            @error("google_map")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="title" class="form-label font-weight-bold">Google Captcha Site Anahtarı</label>
                                            <input  value="{{old('gsm', $option->site_key)}}" class="form-control" type="text" name="site_key" id="title">
                                            @error("site_key")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="title" class="form-label font-weight-bold">Google Captcha Gizli Anahtar</label>
                                            <input  value="{{old('gsm', $option->secret_key)}}" class="form-control" type="text" name="secret_key" id="title">
                                            @error("secret_key")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                {{-- <div class="form-group ">
                                     <label class="form-label">Kullanıcı Rolü</label>
                                     <select class="form-control select2 custom-select" name="role">

                                         <option value="{{$standardUserEnum}}">Standart Kullanıcı</option>
                                         <option value="{{$adminEnum}}">Admin</option>
                                     </select>
                                     @error("role")
                                         <span class="text-danger">{{$message}}</span>
                                     @enderror
                                 </div>--}}
                                <button class="btn btn-primary btn-lg mt-5 mb-5" type="submit">Kaydet</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("js")
    <!-- WYSIWYG Editor js -->
    <script src="{{panelAsset('plugins/wysiwyag/jquery.richtext.js')}}"></script>
    <script src="{{panelAsset('plugins/wysiwyag/richText1.js')}}"></script>
    <script src="{{panelAsset('CustomOperations/editImageChanger.js')}}"></script>
@endpush
